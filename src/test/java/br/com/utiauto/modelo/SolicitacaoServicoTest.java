package br.com.utiauto.modelo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.SolicitacaoServicoTestBuilder;
import br.com.utiauto.dto.solicitacao.SolicitacaoServicoDto;
import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;

public class SolicitacaoServicoTest {


	private SolicitacaoServicoTestBuilder solicitacaoServicoTestBuilder;


	@Before
	public void init() {
		solicitacaoServicoTestBuilder = new SolicitacaoServicoTestBuilder();
	}
	
	@After
	public void end() {
		
	}

	@Test
	public void deveRetornarTodosOsDados() {
		SolicitacaoServico solicitacaoServico = solicitacaoServicoTestBuilder.build();
		assertEquals(1L, solicitacaoServico.getId());
	}
	
	
	@Test
	public void deveRetornarId() {
		SolicitacaoServico solicitacaoServico = solicitacaoServicoTestBuilder.comId(2L).build();
		assertEquals(2L, solicitacaoServico.getId());
	}
	
	@Test
	public void deveRetornarStatus() {
		SolicitacaoServico solicitacaoServico = solicitacaoServicoTestBuilder.comStatusSolicitacao(StatusSolicitacaoServicoEnum.AGUARDANDO_APROVACAO).build();
		assertEquals(StatusSolicitacaoServicoEnum.AGUARDANDO_APROVACAO, solicitacaoServico.getStatus());
	}
	
	
	

}
