package br.com.utiauto.builder;

import br.com.utiauto.modelo.Servico;

public class ServicoTestBuilder {

	private Long id;
	private String nomeServico;
	private String iconeServico;
	private double valorServico;

	public ServicoTestBuilder() {
		this.setId(1L);
		this.nomeServico = "chaveiro";
		this.iconeServico = "Teste";
		this.valorServico = 100;
	}

	public ServicoTestBuilder comId(Long id) {
		this.setId(id);
		return this;
	}

	public ServicoTestBuilder comNomeservico(String nomeServico) {
		this.nomeServico = nomeServico;
		return this;
	}

	public ServicoTestBuilder comIconeservico(String iconeServico) {
		this.iconeServico = iconeServico;
		return this;
	}

	public ServicoTestBuilder comValorservico(double valorServico) {
		this.valorServico = valorServico;
		return this;
	}

	public Servico build() {
		return new Servico(nomeServico, iconeServico, valorServico);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
