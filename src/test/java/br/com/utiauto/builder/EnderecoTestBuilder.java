package br.com.utiauto.builder;

import br.com.utiauto.modelo.Endereco;
import br.com.utiauto.modelo.Usuario;

public class EnderecoTestBuilder {
	
	
	private Long id;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String localidade;
	private String uf;
	private String cep;
	private Usuario usuario;
	
	
	
	public EnderecoTestBuilder() {
		this.usuario = new Usuario();
		this.id = 1L;
		this.logradouro = "Rua Doidona";
		this.numero = "123";
		this.complemento = "Tete";
		this.bairro = "Santana";
		this.localidade = "SP";
		this.uf = "SP";
		this.cep = "02013050";
	}
	
	
	public EnderecoTestBuilder comId(Long id) {
		this.id = id;
		return this;
	}
	
	public EnderecoTestBuilder comLogradouro(String logradouro) {
		this.logradouro = logradouro;
		return this;
	}
	
	public EnderecoTestBuilder comNumero(String numero) {
		this.numero = numero;
		return this;
	}
	
	public EnderecoTestBuilder comComplemento(String complemento) {
		this.complemento = complemento;
		return this;
	}
	
	
	public EnderecoTestBuilder comBairro(String bairro) {
		this.bairro = bairro;
		return this;
	}
	
	public EnderecoTestBuilder comLocalidade(String localidade) {
		this.localidade = localidade;
		return this;
	}

	public EnderecoTestBuilder comUf(String uf) {
		this.uf = uf;
		return this;
	}
	
	public EnderecoTestBuilder comCep(String cep) {
		this.cep = cep;
		return this;
	}
	
	public Endereco build() {
		Endereco endereco = new Endereco(logradouro, numero, complemento, bairro, localidade, uf, cep, usuario);
		endereco.setId(id);
		return endereco;
	}
	
}
