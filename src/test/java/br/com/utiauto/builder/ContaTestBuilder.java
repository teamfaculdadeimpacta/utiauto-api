package br.com.utiauto.builder;

import br.com.utiauto.modelo.Conta;
import br.com.utiauto.modelo.Usuario;

public class ContaTestBuilder {

	private Long id;
	private Long saldo;
	private Usuario prestador;

	public ContaTestBuilder() {
		this.prestador = new Usuario();
		this.id = 1L;
		this.saldo = 200L;
	}

	public ContaTestBuilder comId(Long id) {
		this.id = id;
		return this;
	}

	public ContaTestBuilder comSaldo(Long saldo) {
		this.saldo = saldo;
		return this;
	}
	
	public ContaTestBuilder comPrestador(Usuario prestador) {
		this.prestador = prestador;
		return this;
	}

	public Conta build() {
		Conta conta = new Conta(saldo, prestador);
		conta.setId(id);
		return conta;
	}

}
