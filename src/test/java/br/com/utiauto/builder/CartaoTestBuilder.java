package br.com.utiauto.builder;

import br.com.utiauto.enums.TipoCartaoEnum;
import br.com.utiauto.modelo.Cartao;
import br.com.utiauto.modelo.Usuario;

public class CartaoTestBuilder  {

	private Long nrCartao;
	private String dtVencimento;
	private int codSeg;
	private String pais;
	private TipoCartaoEnum tpCartao;
	
	
	public CartaoTestBuilder() {
		this.nrCartao = 124124L;
		this.dtVencimento = "10/10/2020";
		this.codSeg = 312;
		this.pais = "Brasil";
		this.tpCartao = TipoCartaoEnum.CREDITO;
	}
	
	
	public CartaoTestBuilder comNrCartao(Long nrCartao) {
		this.nrCartao = nrCartao;
		return this;
	}
	
	public CartaoTestBuilder comDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
		return this;
	}
	
	public CartaoTestBuilder comCodSeg(int codSeg) {
		this.codSeg = codSeg;
		return this;
	}
	
	public CartaoTestBuilder comPais(String pais) {
		this.pais = pais;
		return this;
	}
	
	public CartaoTestBuilder comTpCartamEnum(TipoCartaoEnum tpCartao) {
		this.tpCartao = tpCartao;
		return this;
	}
	
	public Cartao build() {
		Usuario usuario = new Usuario();
		return new Cartao(nrCartao, dtVencimento, codSeg, pais, tpCartao, usuario);
	}
	
	
	
}
