package br.com.utiauto.builder;

import br.com.utiauto.modelo.CadastroPagSeguro;
import br.com.utiauto.modelo.Usuario;

public class CadastroPagSeguroBuilder {

	private Long id;
	private String emailPagSeguro;
	private String password;
	private Usuario usuario;

	
	
	public CadastroPagSeguroBuilder() {
		Usuario usuario = new Usuario();
		this.id = 1L;
		this.emailPagSeguro = "teste@teste.com";
		this.password = "1234";
		this.usuario = usuario;
	}
	
	public CadastroPagSeguroBuilder comId(Long id) {
		this.id = id;
		return this;
	}
	
	public CadastroPagSeguroBuilder comEmail(String email) {
		this.emailPagSeguro = email;
		return this;
	}
	
	
	public CadastroPagSeguroBuilder comSenha(String password) {
		this.password = password;
		return this;
	}
	
	public CadastroPagSeguroBuilder comUsuario(Usuario usuario) {
		this.usuario = usuario;
		return this;
	}
	
	
	public CadastroPagSeguro build() {
		CadastroPagSeguro cadastroPagSeguro = new CadastroPagSeguro(this.emailPagSeguro, this.password, this.usuario);
		cadastroPagSeguro.setId(this.id);
		return cadastroPagSeguro;
	}
	

}
