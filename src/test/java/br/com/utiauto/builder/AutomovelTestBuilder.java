package br.com.utiauto.builder;

import br.com.utiauto.modelo.Automovel;
import br.com.utiauto.modelo.Usuario;

public class AutomovelTestBuilder {

	private Long id;
	private String modelo;
	private int ano;
	private String marca;
	private Long renavam;
	private String cor;
	private String placa;
	private Usuario usuario;

	public AutomovelTestBuilder() {
		this.usuario = new Usuario();
		this.id = 1L;
		this.modelo = "GOL";
		this.ano = 2018;
		this.marca = "VOLKSVAGEM";
		this.renavam = 4894984981242L;
		this.cor = "Branco";
		this.placa = "EAF1234";
	}

	public AutomovelTestBuilder comId(Long id) {
		this.id = id;
		return this;
	}

	public AutomovelTestBuilder comModelo(String modelo) {
		this.modelo = modelo;
		return this;
	}

	public AutomovelTestBuilder comAno(int ano) {
		this.ano = ano;
		return this;
	}

	public AutomovelTestBuilder comMarca(String marca) {
		this.marca = marca;
		return this;
	}

	public AutomovelTestBuilder comRenavam(Long renavam) {
		this.renavam = renavam;
		return this;
	}

	public AutomovelTestBuilder comCor(String cor) {
		this.cor = cor;
		return this;
	}

	public AutomovelTestBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	
	public AutomovelTestBuilder comUsuario(Usuario usuario) {
		this.usuario = usuario;
		return this;
	}
	
	
	public Automovel build() {
		Automovel automovel = new Automovel(modelo, ano, marca, renavam, cor, placa, usuario);
		automovel.setId(id);
		return automovel;
	}

}
