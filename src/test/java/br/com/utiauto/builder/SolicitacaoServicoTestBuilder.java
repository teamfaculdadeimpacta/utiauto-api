package br.com.utiauto.builder;

import java.time.LocalDateTime;

import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.enums.TipoDePagamentoEnum;
import br.com.utiauto.modelo.Automovel;
import br.com.utiauto.modelo.EnderecoSolicitacao;
import br.com.utiauto.modelo.Servico;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.modelo.Usuario;

public class SolicitacaoServicoTestBuilder {


	public SolicitacaoServicoTestBuilder() {
		this.id = 1L;
		this.servico = new Servico();
		this.usuario = new Usuario();
		this.enderecoSolicitacao = new EnderecoSolicitacao();
		this.localizacaoInicial = new String[] {"12421421","12421412"};
		this.localizacaoFinal = new String[] {"12421421","12345"};
		this.localizacaoAtual = new String[] {"12421421","123456"};
		this.automovel = new Automovel();
		this.tipoDePagamento = TipoDePagamentoEnum.CARTAO_DE_CREDITO;
		this.status = StatusSolicitacaoServicoEnum.EMANDAMENTO;
		this.dataCriacao = LocalDateTime.now();
	}
	
	
	
	private Long id;

	private Servico servico;

	private Usuario usuario;

	private Usuario prestadorDeServico;

	private EnderecoSolicitacao enderecoSolicitacao;

	private String[] localizacaoInicial;

	private String[] localizacaoFinal;

	private String[] localizacaoAtual;


	private Automovel automovel;

	private TipoDePagamentoEnum tipoDePagamento = TipoDePagamentoEnum.DINHEIRO;

	private StatusSolicitacaoServicoEnum status = StatusSolicitacaoServicoEnum.EMESPERA;

	private LocalDateTime dataCriacao = LocalDateTime.now();
	
	
	public SolicitacaoServicoTestBuilder comId(Long id) {
		this.id = id;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comServico(Servico servico) {
		this.servico = servico;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comUsuario(Usuario usuario) {
		this.usuario = usuario;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comPrestador(Usuario prestador) {
		this.prestadorDeServico = prestador;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comEndereco(EnderecoSolicitacao endereco) {
		this.enderecoSolicitacao = endereco;
		return this;
	}
	
	
	public SolicitacaoServicoTestBuilder comLocalizacaoAtual(String[] localizacaoAtual) {
		this.localizacaoAtual = localizacaoAtual;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comLocalizacaoInicial(String[] localizacaoInicial) {
		this.localizacaoInicial = localizacaoInicial;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comLocalizacaoFinal(String[] localizacaoFinal) {
		this.localizacaoFinal = localizacaoFinal;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comAutomovel(Automovel automovel) {
		this.automovel = automovel;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comTipoDePagamento(TipoDePagamentoEnum tipoDePagamento) {
		this.tipoDePagamento = tipoDePagamento;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comStatusSolicitacao(StatusSolicitacaoServicoEnum status) {
		this.status = status;
		return this;
	}
	
	public SolicitacaoServicoTestBuilder comData(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
		return this;
	}
	
	public SolicitacaoServico build() {
		SolicitacaoServico solicitacaoServico = new SolicitacaoServico(servico, usuario, enderecoSolicitacao, automovel, status, tipoDePagamento, localizacaoFinal);
		solicitacaoServico.setId(id);
		solicitacaoServico.setLocalizacaoAtual(localizacaoAtual);
		solicitacaoServico.setLocalizacaoInicial(localizacaoInicial);
		solicitacaoServico.setPrestadorDeServico(prestadorDeServico);
		solicitacaoServico.setDataCriacao(dataCriacao);
		return solicitacaoServico;
	}
	
}
