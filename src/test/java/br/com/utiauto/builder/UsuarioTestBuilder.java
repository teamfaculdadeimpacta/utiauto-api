package br.com.utiauto.builder;

import java.util.ArrayList;
import java.util.List;

import br.com.utiauto.modelo.Perfil;
import br.com.utiauto.modelo.Servico;
import br.com.utiauto.modelo.Usuario;

public class UsuarioTestBuilder {

	private Long id;
	private String nome;
	private String sobrenome;
	private String cpf;
	private String rg;
	private String dataNascimento;
	private String email;
	private String senha;
	private String telefone;
	private String celular;
	private String cnh;
	private Perfil perfil;

	public UsuarioTestBuilder() {
		this.setId(1L);
		this.nome = "Silas Doidao";
		this.sobrenome = "Garcia";
		this.cpf = "40173526830";
		this.rg = "493861616";
		this.dataNascimento = "03/12/1989";
		this.email = "silas@teste.com";
		this.senha = "123465";
		this.telefone = "1195566969";
		this.celular = "119556969";
		this.cnh = "41564654";
		this.perfil = populaServico();

	}

	private void setId(long l) {
		// TODO Auto-generated method stub

	}

	public UsuarioTestBuilder comId(Long id) {
		this.setId(id);
		return this;
	}

	public UsuarioTestBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public UsuarioTestBuilder comSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
		return this;
	}

	public UsuarioTestBuilder comCPF(String cpf) {
		this.cpf = cpf;
		return this;
	}

	public UsuarioTestBuilder comRG(String rg) {
		this.rg = rg;
		return this;
	}

	public UsuarioTestBuilder comDatanascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
		return this;
	}

	public UsuarioTestBuilder comEmail(String email) {
		this.email = email;
		return this;
	}

	public UsuarioTestBuilder comSenha(String senha) {
		this.senha = senha;
		return this;
	}

	public UsuarioTestBuilder comTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public UsuarioTestBuilder comCelular(String celular) {
		this.celular = celular;
		return this;
	}

	public UsuarioTestBuilder comCNH(String cnh) {
		this.cnh = cnh;
		return this;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Perfil populaServico() {
		Perfil perfil = new Perfil();
		List<Servico> servico = new ArrayList<>();
		servico.add(new Servico("Teste", "teste", 200.0));
		perfil.setServico(servico);
		return perfil;
	}
	
	public Usuario build() {
		return new Usuario(nome, sobrenome, cpf, rg, dataNascimento, email, senha, telefone, celular, cnh, perfil);
	}

}
