package br.com.utiauto.controller.dto.automovel;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.AutomovelTestBuilder;
import br.com.utiauto.dto.automovel.DetalhesDoAutomovelDto;
import br.com.utiauto.modelo.Automovel;

public class DetalhesDoAutomovelDtoTest {
	
	private AutomovelTestBuilder automovelDtoTestBuilder;

	@Before
	public void init() {
		automovelDtoTestBuilder = new AutomovelTestBuilder();
	}

	@After
	public void end() {

	}

	@Test
	public void deveRetornarIdUm() {
		Automovel carro = automovelDtoTestBuilder.comId(1L).build();
		DetalhesDoAutomovelDto automovelDto = new DetalhesDoAutomovelDto(carro);
		assertEquals(1L, automovelDto.getId());
	}

	@Test
	public void deveRetornarRetornarModelo() {
		Automovel carro = automovelDtoTestBuilder.comModelo("BRASILIA").build();
		DetalhesDoAutomovelDto automovelDto = new DetalhesDoAutomovelDto(carro);
		assertEquals("BRASILIA", automovelDto.getModelo());
	}

	@Test
	public void deveRetornarAno() {
		Automovel carro = automovelDtoTestBuilder.comAno(2019).build();
		DetalhesDoAutomovelDto automovelDto = new DetalhesDoAutomovelDto(carro);
		assertEquals(2019, automovelDto.getAno());
	}

	@Test
	public void deveRetornarMarca() {
		Automovel carro = automovelDtoTestBuilder.comMarca("VOLKS").build();
		DetalhesDoAutomovelDto automovelDto = new DetalhesDoAutomovelDto(carro);
		assertEquals("VOLKS", automovelDto.getMarca());
	}

	@Test
	public void deveRetornarRenavam() {
		Automovel carro = automovelDtoTestBuilder.comRenavam(14521521L).build();
		DetalhesDoAutomovelDto automovelDto = new DetalhesDoAutomovelDto(carro);
		assertEquals(14521521L, automovelDto.getRenavam());
	}

	@Test
	public void deveRetornarACor() {
		Automovel carro = automovelDtoTestBuilder.comCor("AMARELO").build();
		DetalhesDoAutomovelDto automovelDto = new DetalhesDoAutomovelDto(carro);
		assertEquals("AMARELO", automovelDto.getCor());
	}
	
	@Test
	public void deveRetornarPlaca() {
		Automovel carro = automovelDtoTestBuilder.comPlaca("EAF1234").build();
		DetalhesDoAutomovelDto automovelDto = new DetalhesDoAutomovelDto(carro);
		assertEquals("EAF1234", automovelDto.getPlaca());
	}
	
	

}
