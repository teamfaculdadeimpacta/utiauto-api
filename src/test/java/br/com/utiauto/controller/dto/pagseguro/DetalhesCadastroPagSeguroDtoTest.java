package br.com.utiauto.controller.dto.pagseguro;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.CadastroPagSeguroBuilder;
import br.com.utiauto.dto.pagseguro.DetalhesCadastroPagSeguroDto;
import br.com.utiauto.modelo.CadastroPagSeguro;

public class DetalhesCadastroPagSeguroDtoTest {
	
	
	private CadastroPagSeguroBuilder cadastroPagSeguroBuilder;

	@Before
	public void init() {
		cadastroPagSeguroBuilder = new CadastroPagSeguroBuilder();
	}

	@After
	public void end() {

	}


	@Test
	public void deverRetornarDadosCompletos() {
		CadastroPagSeguro cadastroPagSeguro = cadastroPagSeguroBuilder.build();
		DetalhesCadastroPagSeguroDto cadastroPagSeguroDto = new DetalhesCadastroPagSeguroDto(cadastroPagSeguro);
		assertEquals("teste@teste.com", cadastroPagSeguroDto.getEmailPagSeguro());
		assertEquals("1234", cadastroPagSeguroDto.getPassword());

	}

	@Test
	public void deveRetornarEmail() {
		CadastroPagSeguro cadastroPagSeguro = cadastroPagSeguroBuilder.comEmail("ola@ola").build();
		DetalhesCadastroPagSeguroDto cadastroPagSeguroDto = new DetalhesCadastroPagSeguroDto(cadastroPagSeguro);
		assertEquals("ola@ola", cadastroPagSeguroDto.getEmailPagSeguro());

	}

	@Test
	public void deveRetornarSenha() {
		CadastroPagSeguro cadastroPagSeguro = cadastroPagSeguroBuilder.comSenha("ola@ola").build();
		DetalhesCadastroPagSeguroDto cadastroPagSeguroDto = new DetalhesCadastroPagSeguroDto(cadastroPagSeguro);
		assertEquals("ola@ola", cadastroPagSeguroDto.getPassword());
	}

	@Test
	public void deveRetornarLista() {
		CadastroPagSeguro cadastroPagSeguro1 = cadastroPagSeguroBuilder.build();
		CadastroPagSeguro cadastroPagSeguro2 = cadastroPagSeguroBuilder.build();
		
		List<CadastroPagSeguro> cadastro = new ArrayList<>();
		cadastro.add(cadastroPagSeguro2);
		cadastro.add(cadastroPagSeguro1);
		List<DetalhesCadastroPagSeguroDto> lista = DetalhesCadastroPagSeguroDto.converter(cadastro);
		
		assertEquals(2, lista.size());

	}
	
	
	

}
