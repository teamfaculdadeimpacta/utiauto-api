package br.com.utiauto.controller.dto.servico;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.ServicoTestBuilder;
import br.com.utiauto.dto.servico.DetalhesDoServicoDto;
import br.com.utiauto.modelo.Servico;

public class DetalhesDoServicoDtoTest {
	
	private ServicoTestBuilder servicoDtoTestBuilder;

	@Before
	public void init() {
		servicoDtoTestBuilder = new ServicoTestBuilder();
	}

	@After
	public void end() {

	}

	@Test
	public void deveRetornarRetornarNome() {
		Servico servico = servicoDtoTestBuilder.comNomeservico("CHAVEIRO").build();
		DetalhesDoServicoDto servicoDto = new DetalhesDoServicoDto(servico);
		assertEquals("CHAVEIRO", servicoDto.getNomeServico());
	}
	
	
	@Test
	public void naoDeveRetornarRetornarNomeCorreto() {
		Servico servico = servicoDtoTestBuilder.comNomeservico("CHAVEIRO").build();
		DetalhesDoServicoDto servicoDto = new DetalhesDoServicoDto(servico);
		assertNotEquals("Vanderlei", servicoDto.getNomeServico());
	}

	@Test
	public void deveRetornarSobrenome() {
		Servico servico = servicoDtoTestBuilder.comIconeservico("TESTE").build();
		DetalhesDoServicoDto servicoDto = new DetalhesDoServicoDto(servico);
		assertEquals("TESTE", servicoDto.getIconeServico());
	}

	@Test
	public void deveRetornarValorDeServico() {
		Servico servico = servicoDtoTestBuilder.comValorservico(200.0).build();
		DetalhesDoServicoDto servicoDto = new DetalhesDoServicoDto(servico);
		assertEquals(200.0, servicoDto.getValorServico(), 0.0000);
	}		
	
	

}
