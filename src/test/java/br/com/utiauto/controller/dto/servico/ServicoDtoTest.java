package br.com.utiauto.controller.dto.servico;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.ServicoTestBuilder;
import br.com.utiauto.dto.servico.ServicoDto;
import br.com.utiauto.modelo.Servico;

public class ServicoDtoTest {


	private ServicoTestBuilder servicoDtoTestBuilder;

	@Before
	public void init() {
		servicoDtoTestBuilder = new ServicoTestBuilder();
	}

	@After
	public void end() {

	}


	@Test
	public void deveRetornarRetornarNome() {
		Servico servico = servicoDtoTestBuilder.comNomeservico("VANDERLEI").build();
		ServicoDto servicoDto = new ServicoDto(servico);
		assertEquals("VANDERLEI", servicoDto.getNomeServico());
	}

	@Test
	public void deveRetornarSobrenome() {
		Servico servico = servicoDtoTestBuilder.comIconeservico("TESTE").build();
		ServicoDto servicoDto = new ServicoDto(servico);
		assertEquals("TESTE", servicoDto.getIconeServico());
	}

	@Test
	public void deveRetornarValorDoServico() {
		Servico servico = servicoDtoTestBuilder.comValorservico(200.0).build();
		ServicoDto servicoDto = new ServicoDto(servico);
		assertEquals(200.0, servicoDto.getValorServico(),0.0000);
	}	
	
	
	@Test
	public void deveRetornarListaDeServicoComResultadoUm() {
		List<Servico> lista = new ArrayList<Servico>();
		Servico servico = servicoDtoTestBuilder.build();
		lista.add(servico);
		List<ServicoDto> servicos = ServicoDto.converter(lista);
		assertEquals(1, servicos.size());
	}
	
	
	@Test
	public void deveRetornarListaDeServicoComResultadoDois() {
		List<Servico> lista = new ArrayList<Servico>();
		Servico servico = servicoDtoTestBuilder.build();
		Servico servico2 = servicoDtoTestBuilder.build();
		lista.add(servico);
		lista.add(servico2);
		List<ServicoDto> servicos = ServicoDto.converter(lista);
		assertEquals(2, servicos.size());
	}

}
