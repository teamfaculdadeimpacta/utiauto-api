package br.com.utiauto.controller.dto.automovel;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.AutomovelTestBuilder;
import br.com.utiauto.dto.automovel.AutomovelDto;
import br.com.utiauto.modelo.Automovel;

public class AutomovelDtoTest {


	private AutomovelTestBuilder automovelDtoTestBuilder;

	@Before
	public void init() {
		automovelDtoTestBuilder = new AutomovelTestBuilder();
	}

	@After
	public void end() {

	}

	@Test
	public void deveRetornarIdUm() {
		Automovel carro = automovelDtoTestBuilder.comId(1L).build();
		AutomovelDto automovelDto = new AutomovelDto(carro);
		assertEquals(1L, automovelDto.getId());
	}

	@Test
	public void deveRetornarRetornarModelo() {
		Automovel carro = automovelDtoTestBuilder.comModelo("BRASILIA").build();
		AutomovelDto automovelDto = new AutomovelDto(carro);
		assertEquals("BRASILIA", automovelDto.getModelo());
	}

	@Test
	public void deveRetornarAno() {
		Automovel carro = automovelDtoTestBuilder.comAno(2019).build();
		AutomovelDto automovelDto = new AutomovelDto(carro);
		assertEquals(2019, automovelDto.getAno());
	}

	@Test
	public void deveRetornarMarca() {
		Automovel carro = automovelDtoTestBuilder.comMarca("VOLKS").build();
		AutomovelDto automovelDto = new AutomovelDto(carro);
		assertEquals("VOLKS", automovelDto.getMarca());
	}

	@Test
	public void deveRetornarRenavam() {
		Automovel carro = automovelDtoTestBuilder.comRenavam(14521521L).build();
		AutomovelDto automovelDto = new AutomovelDto(carro);
		assertEquals(14521521L, automovelDto.getRenavam());
	}

	@Test
	public void deveRetornarACor() {
		Automovel carro = automovelDtoTestBuilder.comCor("AMARELO").build();
		AutomovelDto automovelDto = new AutomovelDto(carro);
		assertEquals("AMARELO", automovelDto.getCor());
	}
	
	@Test
	public void deveRetornarPlaca() {
		Automovel carro = automovelDtoTestBuilder.comPlaca("EAF1234").build();
		AutomovelDto automovelDto = new AutomovelDto(carro);
		assertEquals("EAF1234", automovelDto.getPlaca());
	}
	
	
	@Test
	public void deveRetornarListaDeAutomovelComResultadoUm() {
		List<Automovel> lista = new ArrayList<Automovel>();
		Automovel carro = automovelDtoTestBuilder.build();
		lista.add(carro);
		List<AutomovelDto> carros = AutomovelDto.converter(lista);
		assertEquals(1, carros.size());
	}
	
	
	@Test
	public void deveRetornarListaDeAutomovelComResultadoDois() {
		List<Automovel> lista = new ArrayList<Automovel>();
		Automovel carro = automovelDtoTestBuilder.build();
		Automovel carro2 = automovelDtoTestBuilder.build();
		lista.add(carro);
		lista.add(carro2);
		List<AutomovelDto> carros = AutomovelDto.converter(lista);
		assertEquals(2, carros.size());
	}

}
