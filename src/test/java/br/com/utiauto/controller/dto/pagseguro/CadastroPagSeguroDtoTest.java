package br.com.utiauto.controller.dto.pagseguro;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.CadastroPagSeguroBuilder;
import br.com.utiauto.dto.pagseguro.CadastroPagSeguroDto;
import br.com.utiauto.modelo.CadastroPagSeguro;

public class CadastroPagSeguroDtoTest {

	private CadastroPagSeguroBuilder cadastroPagSeguroBuilder;

	@Before
	public void init() {
		cadastroPagSeguroBuilder = new CadastroPagSeguroBuilder();
	}

	@After
	public void end() {

	}

	@Test
	public void deveRetornarIdZero() {
		CadastroPagSeguro cadastroPagSeguro = cadastroPagSeguroBuilder.comId(0L).build();
		CadastroPagSeguroDto cadastroPagSeguroDto = new CadastroPagSeguroDto(cadastroPagSeguro);
		assertEquals(0L, cadastroPagSeguroDto.getId(), 0.0000);
	}

	@Test
	public void deverRetornarDadosCompletos() {
		CadastroPagSeguro cadastroPagSeguro = cadastroPagSeguroBuilder.build();
		CadastroPagSeguroDto cadastroPagSeguroDto = new CadastroPagSeguroDto(cadastroPagSeguro);

		assertEquals(1L, cadastroPagSeguroDto.getId());
		assertEquals("teste@teste.com", cadastroPagSeguroDto.getEmailPagSeguro());
		assertEquals("1234", cadastroPagSeguroDto.getPassword());

	}

	@Test
	public void deveRetornarEmail() {
		CadastroPagSeguro cadastroPagSeguro = cadastroPagSeguroBuilder.comEmail("ola@ola").build();
		CadastroPagSeguroDto cadastroPagSeguroDto = new CadastroPagSeguroDto(cadastroPagSeguro);
		assertEquals("ola@ola", cadastroPagSeguroDto.getEmailPagSeguro());

	}

	@Test
	public void deveRetornarSenha() {
		CadastroPagSeguro cadastroPagSeguro = cadastroPagSeguroBuilder.comSenha("ola@ola").build();
		CadastroPagSeguroDto cadastroPagSeguroDto = new CadastroPagSeguroDto(cadastroPagSeguro);
		assertEquals("ola@ola", cadastroPagSeguroDto.getPassword());
	}

	@Test
	public void deveRetornarLista() {
		CadastroPagSeguro cadastroPagSeguro1 = cadastroPagSeguroBuilder.build();
		CadastroPagSeguro cadastroPagSeguro2 = cadastroPagSeguroBuilder.build();
		
		List<CadastroPagSeguro> cadastro = new ArrayList<>();
		cadastro.add(cadastroPagSeguro2);
		cadastro.add(cadastroPagSeguro1);
		List<CadastroPagSeguroDto> lista = CadastroPagSeguroDto.converter(cadastro);
		
		assertEquals(2, lista.size());

	}

}
