package br.com.utiauto.controller.dto.usuario;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.utiauto.builder.UsuarioTestBuilder;
import br.com.utiauto.dto.usuario.UsuarioDto;
import br.com.utiauto.modelo.Usuario;

public class UsuarioDtoTest {


	private UsuarioTestBuilder usuarioDtoTestBuilder;

	@Before
	public void init() {
		usuarioDtoTestBuilder = new UsuarioTestBuilder();
	}

	@After
	public void end() {

	}


	@Test
	public void deveRetornarRetornarNome() {
		Usuario pessoa = usuarioDtoTestBuilder.comNome("VANDERLEI").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("VANDERLEI", usuarioDto.getNome());
	}

	@Test
	public void deveRetornarSobrenome() {
		Usuario pessoa = usuarioDtoTestBuilder.comSobrenome("SILVA").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("SILVA", usuarioDto.getSobrenome());
	}

	@Test
	public void deveRetornarCpf() {
		Usuario pessoa = usuarioDtoTestBuilder.comCPF("22873582855").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("22873582855", usuarioDto.getCpf());
	}	

	@Test
	public void deveRetornarRg() {
		Usuario pessoa = usuarioDtoTestBuilder.comRG("406840477").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("406840477", usuarioDto.getRg());
	}		
	
	
	@Test
	public void deveRetornarDatanascimento() {
		Usuario pessoa = usuarioDtoTestBuilder.comDatanascimento("31/12/1986").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("31/12/1986", usuarioDto.getDataNascimento());
	}	
	
	@Test
	public void deveRetornarEmail() {
		Usuario pessoa = usuarioDtoTestBuilder.comEmail("vanderlei@teste.com.br").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("vanderlei@teste.com.br", usuarioDto.getEmail());
	}	
	
	@Test
	public void deveRetornarTelefone() {
		Usuario pessoa = usuarioDtoTestBuilder.comTelefone("SILVA").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("SILVA", usuarioDto.getTelefone());
	}		
	
	@Test
	public void deveRetornarCelular() {
		Usuario pessoa = usuarioDtoTestBuilder.comCelular("998765432").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("998765432", usuarioDto.getCelular());
	}		
	
	@Test
	public void deveRetornarCNH() {
		Usuario pessoa = usuarioDtoTestBuilder.comCNH("01234567891").build();
		UsuarioDto usuarioDto = new UsuarioDto(pessoa);
		assertEquals("01234567891", usuarioDto.getCnh());
	}	

	
	
	@Test
	public void deveRetornarListaDeUsuarioComResultadoUm() {
		List<Usuario> lista = new ArrayList<Usuario>();
		Usuario pessoa = usuarioDtoTestBuilder.build();
		lista.add(pessoa);
		List<UsuarioDto> pessoas = UsuarioDto.converter(lista);
		assertEquals(1, pessoas.size());
	}
	
	
	@Test
	public void deveRetornarListaDeUsuarioComResultadoDois() {
		List<Usuario> lista = new ArrayList<Usuario>();
		Usuario pessoa = usuarioDtoTestBuilder.build();
		Usuario pessoa2 = usuarioDtoTestBuilder.build();
		lista.add(pessoa);
		lista.add(pessoa2);
		List<UsuarioDto> pessoas = UsuarioDto.converter(lista);
		assertEquals(2, pessoas.size());
	}

}
