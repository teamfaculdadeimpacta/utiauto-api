package br.com.utiauto.controller.dto.endereco;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.EnderecoTestBuilder;
import br.com.utiauto.dto.endereco.DetalhesDoEnderecoDto;
import br.com.utiauto.modelo.Endereco;

public class DetalhesDoEnderecoDtoTest {

	private EnderecoTestBuilder enderecoTestBuilder;

	@Before
	public void init() {
		enderecoTestBuilder = new EnderecoTestBuilder();
	}

	@After
	public void end() {

	}

	@Test
	public void deveRetornarIdZerado() {
		Endereco endereco = enderecoTestBuilder.comId(0L).build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals(0L, enderecoDto.getId(), 0.00000);
	}
	
	@Test
	public void deveRetornarId() {
		Endereco endereco = enderecoTestBuilder.comId(1L).build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals(1L, enderecoDto.getId(), 0.00000);
	}
	
	@Test
	public void deveRetornarLogradouroVazio() {
		Endereco endereco = enderecoTestBuilder.comLogradouro("").build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals("", enderecoDto.getLogradouro());
	}
	
	@Test
	public void deveRetornarLogradouro() {
		Endereco endereco = enderecoTestBuilder.comLogradouro("Rua Jose").build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals("Rua Jose", enderecoDto.getLogradouro());
	}
	
	@Test
	public void deveRetornarNumero() {
		Endereco endereco = enderecoTestBuilder.comNumero("1234").build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals("1234", enderecoDto.getNumero());
	}
	
	@Test
	public void deveRetornarComplemento() {
		Endereco endereco = enderecoTestBuilder.comComplemento("TESTE A").build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals("TESTE A", enderecoDto.getComplemento());
	}
	
	@Test
	public void deveRetornarBairro() {
		Endereco endereco = enderecoTestBuilder.comBairro("santana").build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals("santana", enderecoDto.getBairro());
	}
	
	
	@Test
	public void deveRetornarLocalidade() {
		Endereco endereco = enderecoTestBuilder.comLocalidade("São Paulo").build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals("São Paulo", enderecoDto.getLocalidade());
	}
	
	@Test
	public void deveRetornarUF() {
		Endereco endereco = enderecoTestBuilder.comUf("SP").build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals("SP", enderecoDto.getUf());
	}
	
	
	@Test
	public void deveRetornarCep() {
		Endereco endereco = enderecoTestBuilder.comCep("02013050").build();
		DetalhesDoEnderecoDto enderecoDto = new DetalhesDoEnderecoDto(endereco);
		assertEquals("02013050", enderecoDto.getCep());
	}
	
	


}
