package br.com.utiauto.controller.dto.endereco;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.EnderecoTestBuilder;
import br.com.utiauto.dto.endereco.EnderecoDto;
import br.com.utiauto.modelo.Endereco;

public class EnderecoDtoTest {

	private EnderecoTestBuilder enderecoTestBuilder;

	@Before
	public void init() {
		enderecoTestBuilder = new EnderecoTestBuilder();
	}

	@After
	public void end() {

	}

	@Test
	public void deveRetornarIdZerado() {
		Endereco endereco = enderecoTestBuilder.comId(0L).build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals(0L, enderecoDto.getId(), 0.00000);
	}
	
	@Test
	public void deveRetornarId() {
		Endereco endereco = enderecoTestBuilder.comId(1L).build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals(1L, enderecoDto.getId(), 0.00000);
	}
	
	@Test
	public void deveRetornarLogradouroVazio() {
		Endereco endereco = enderecoTestBuilder.comLogradouro("").build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals("", enderecoDto.getLogradouro());
	}
	
	@Test
	public void deveRetornarLogradouro() {
		Endereco endereco = enderecoTestBuilder.comLogradouro("Rua Jose").build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals("Rua Jose", enderecoDto.getLogradouro());
	}
	
	@Test
	public void deveRetornarNumero() {
		Endereco endereco = enderecoTestBuilder.comNumero("1234").build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals("1234", enderecoDto.getNumero());
	}
	
	@Test
	public void deveRetornarComplemento() {
		Endereco endereco = enderecoTestBuilder.comComplemento("TESTE A").build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals("TESTE A", enderecoDto.getComplemento());
	}
	
	@Test
	public void deveRetornarBairro() {
		Endereco endereco = enderecoTestBuilder.comBairro("santana").build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals("santana", enderecoDto.getBairro());
	}
	
	
	@Test
	public void deveRetornarLocalidade() {
		Endereco endereco = enderecoTestBuilder.comLocalidade("São Paulo").build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals("São Paulo", enderecoDto.getLocalidade());
	}
	
	@Test
	public void deveRetornarUF() {
		Endereco endereco = enderecoTestBuilder.comUf("SP").build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals("SP", enderecoDto.getUf());
	}
	
	
	@Test
	public void deveRetornarCep() {
		Endereco endereco = enderecoTestBuilder.comCep("02013050").build();
		EnderecoDto enderecoDto = new EnderecoDto(endereco);
		assertEquals("02013050", enderecoDto.getCep());
	}
	
	
	@Test
	public void deveRetornarListaDeEnderecoComUmEndereco() {
		List<Endereco> lista = new ArrayList<Endereco>();
		Endereco endereco = enderecoTestBuilder.comCep("02013050").build();
		lista.add(endereco);
		List<EnderecoDto> enderecos = EnderecoDto.converter(lista);
		assertEquals(1, enderecos.size());
	}
	
	@Test
	public void deveRetornarListaDeEnderecoComDoisEnderecos() {
		List<Endereco> lista = new ArrayList<Endereco>();
		Endereco endereco = enderecoTestBuilder.comCep("02013050").build();
		Endereco endereco2 = enderecoTestBuilder.comCep("02013053").build();
		lista.add(endereco);
		lista.add(endereco2);
		List<EnderecoDto> enderecos = EnderecoDto.converter(lista);
		assertEquals(2, enderecos.size());
	}
	


}
