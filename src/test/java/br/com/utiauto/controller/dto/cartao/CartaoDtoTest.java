package br.com.utiauto.controller.dto.cartao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.CartaoTestBuilder;
import br.com.utiauto.dto.cartao.CartaoDto;
import br.com.utiauto.enums.TipoCartaoEnum;
import br.com.utiauto.modelo.Cartao;

public class CartaoDtoTest {

	private CartaoDto cartaoDto;
	private CartaoTestBuilder cartaoDtoTestBuilder;

	@Before
	public void init() {
		cartaoDtoTestBuilder = new CartaoTestBuilder();

	}

	@After
	public void end() {

	}

	
	@Test
	public void deveRetornarNrCartao() {
		Cartao cartao = cartaoDtoTestBuilder.comNrCartao(124142214L).build();
		cartaoDto = new CartaoDto(cartao);
		assertEquals(cartaoDto.getNrCartao(), 124142214L);
	}
	
	@Test
	public void deveRetornarPais() {
		Cartao cartao = cartaoDtoTestBuilder.comPais("Brasil").build();
		cartaoDto = new CartaoDto(cartao);
		assertEquals(cartaoDto.getPais(), "Brasil");
	}
	
	@Test
	public void deveRetornarTipoCartaoEnum() {
		Cartao cartao = cartaoDtoTestBuilder.comTpCartamEnum(TipoCartaoEnum.CREDITO).build();
		cartaoDto = new CartaoDto(cartao);
		assertEquals(cartaoDto.getTpCartao(), TipoCartaoEnum.CREDITO);
	}

	@Test
	public void deveRetornarDataVencimento() {
		Cartao cartao = cartaoDtoTestBuilder.comDtVencimento("10/10/2020").build();
		cartaoDto = new CartaoDto(cartao);
		assertEquals(cartaoDto.getDtVencimento(), "10/10/2020");
	}

	@Test
	public void deveRetornarCodSeg() {
		Cartao cartao = cartaoDtoTestBuilder.comCodSeg(123).build();
		cartaoDto = new CartaoDto(cartao);
		assertEquals(cartaoDto.getCodSeg(), 123);
	}
	
	@Test
	public void deveRetornarCodSegNulo() {
		Cartao cartao = cartaoDtoTestBuilder.comCodSeg(123).build();
		cartaoDto = new CartaoDto(cartao);
		assertEquals(cartaoDto.getCodSeg(), 123);
	}
	
	@Test
	public void deveRetornarListaComUmUnicoValor() {
		List<Cartao> cartoes = new ArrayList<Cartao>();
		Cartao cartao = cartaoDtoTestBuilder.build();
		cartoes.add(cartao);
		List<CartaoDto> cartoesDto = CartaoDto.converter(cartoes);
		assertEquals(1, cartoesDto.size());
	}

}
