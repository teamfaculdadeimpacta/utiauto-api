package br.com.utiauto.controller.dto.conta;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.utiauto.builder.ContaTestBuilder;
import br.com.utiauto.dto.conta.ContaDto;
import br.com.utiauto.modelo.Conta;

public class ContaDtoTest {

	private ContaDto contaDto;
	private ContaTestBuilder contaDtoTestBuilder;

	@Before
	public void init() {
		contaDtoTestBuilder = new ContaTestBuilder();

	}

	@After
	public void end() {

	}

	@Test
	public void deveRetornarId() {
		Conta conta = contaDtoTestBuilder.comId(1L).build();
		contaDto = new ContaDto(conta);
		assertEquals(1, contaDto.getId());
	}

	@Test
	public void deveRetornarSaldoZero() {
		Conta conta = contaDtoTestBuilder.comSaldo(0L).build();
		ContaDto contaDto = new ContaDto(conta);
		assertEquals(0L, contaDto.getSaldo(), 0.0000);
	}

	@Test
	public void deveRetornarSaldoUm() {
		Conta conta = contaDtoTestBuilder.comSaldo(1L).build();
		ContaDto contaDto = new ContaDto(conta);
		assertEquals(1L, contaDto.getSaldo(), 0.0000);
	}

	@Test
	public void deveRetornarListaDeContaDto() {
		List<Conta> lista = new ArrayList<Conta>();

		Conta conta = contaDtoTestBuilder
				.comSaldo(1L)
				.build();

		lista.add(conta);
		List<ContaDto> contas = ContaDto.converter(lista);

		assertEquals(1, contas.size());
	}

}
