package br.com.utiauto.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.utiauto.modelo.CadastroPagSeguro;

public interface CadastroPagSeguroRepository extends JpaRepository<CadastroPagSeguro, Long> {


	List<CadastroPagSeguro> findByUsuario_Id(Long idUsuario);

	Optional<CadastroPagSeguro> findByUsuarioId(Long id);
	

}
