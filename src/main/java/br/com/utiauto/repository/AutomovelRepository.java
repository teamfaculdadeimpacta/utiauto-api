package br.com.utiauto.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.utiauto.modelo.Automovel;

public interface AutomovelRepository extends JpaRepository<Automovel, Long> {

	List<Automovel> findByUsuario_Id(Long id);

	Optional<Automovel> findByUsuarioId(Long id);

}
