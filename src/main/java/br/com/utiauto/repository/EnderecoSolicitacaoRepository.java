package br.com.utiauto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.utiauto.modelo.EnderecoSolicitacao;

public interface EnderecoSolicitacaoRepository extends JpaRepository<EnderecoSolicitacao, Long> {

}
