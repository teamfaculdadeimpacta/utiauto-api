package br.com.utiauto.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.utiauto.modelo.Endereco;
import br.com.utiauto.modelo.Usuario;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {

	Optional<Endereco> findByUsuario(Usuario usuario);

	Optional<Endereco> findByUsuarioId(Long id);

}
