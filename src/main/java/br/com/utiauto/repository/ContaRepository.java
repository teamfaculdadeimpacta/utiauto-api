package br.com.utiauto.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.utiauto.modelo.Conta;

public interface ContaRepository extends JpaRepository<Conta, Long> {

	Optional<Conta> findByPrestadorId(Long id);

}
