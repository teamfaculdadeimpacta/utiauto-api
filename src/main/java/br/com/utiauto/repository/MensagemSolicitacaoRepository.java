package br.com.utiauto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.utiauto.modelo.MensagemSolicitacao;

public interface MensagemSolicitacaoRepository extends JpaRepository<MensagemSolicitacao, Long>{


	List<MensagemSolicitacao> findBySolicitacaoServicoId(Long idSolicitacao);

	
}
