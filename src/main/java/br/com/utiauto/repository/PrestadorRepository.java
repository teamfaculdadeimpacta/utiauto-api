package br.com.utiauto.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.utiauto.modelo.Usuario;

public interface PrestadorRepository extends JpaRepository<Usuario, Long> {

	@Query("SELECT t FROM Usuario t WHERE t.id = :id_prestador AND t.perfil.id = 2")
	Optional<Usuario> findByIdEPerfil(@Param("id_prestador") Long idPrestador);


}
