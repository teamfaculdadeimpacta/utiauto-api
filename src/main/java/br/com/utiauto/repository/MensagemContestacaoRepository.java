package br.com.utiauto.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.modelo.MensagemContestacao;

public interface MensagemContestacaoRepository extends JpaRepository<MensagemContestacao, Long>{

	Optional<MensagemContestacao> findBySolicitacaoServico_id(Long id);

	Optional<MensagemContestacao> findBySolicitacaoServicoUsuarioId(Long idUsuario);

	List<MensagemContestacao> findByTipoMensagem(TipoMensagemEnum ajuda);

}
