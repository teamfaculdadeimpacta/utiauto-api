package br.com.utiauto.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.modelo.SolicitacaoServico;

public interface SolicitaServicoRepository extends JpaRepository<SolicitacaoServico, Long> {

	Optional<SolicitacaoServico> findByUsuarioId(Long id);

	Optional<SolicitacaoServico> findByPrestadorDeServico_Id(Long idPrestador);

	List<SolicitacaoServico> findByServicoId(Long solicitacaoServico);

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.usuario.id = u.id WHERE u.id = :id_usuario AND t.status = :status")
	List<SolicitacaoServico> findByIdStatus(@Param("id_usuario") Long id_usuario,
			@Param("status") StatusSolicitacaoServicoEnum status);

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.prestadorDeServico.id = u.id WHERE u.id = :id_prestador AND t.status = :status")
	List<SolicitacaoServico> findByIdPrestadorStatus(@Param("id_prestador") Long id_prestador,
			@Param("status") StatusSolicitacaoServicoEnum status);

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.prestadorDeServico.id = u.id WHERE u.id = :id_prestador AND t.status = :status AND t.dataCriacao > now() - 1")
	List<SolicitacaoServico> findByIdPrestadorDia(@Param("id_prestador") Long idPrestador,
			@Param("status") StatusSolicitacaoServicoEnum status);

	// TODO arrumar esta coisa horrível
	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.prestadorDeServico.id = u.id WHERE u.id = :id_prestador AND t.status = :status AND t.dataCriacao > TIMESTAMPDIFF(WEEK, 1,CURRENT_DATE)")
	List<SolicitacaoServico> findByIdPrestadorSemana(@Param("id_prestador") Long idPrestador,
			@Param("status") StatusSolicitacaoServicoEnum status);

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.prestadorDeServico.id = u.id WHERE u.id = :id_prestador AND t.status = :status")
	Optional<SolicitacaoServico> findByPrestadorDeServicoIdEmAndamento(@Param("id_prestador") Long idPrestador,
			@Param("status") StatusSolicitacaoServicoEnum status);

	/**
	 * Query de busca de usuáriio com status em andamento, em espera e aguardando
	 * 
	 * @param idUsuario
	 * @param status
	 * @param status2
	 * @param status3
	 * @return
	 */

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.usuario.id = u.id WHERE u.id = :id_usuario AND (t.status = :emAndamento OR t.status = :emEspera OR t.status = :aguardandoAprovacao)")
	Optional<SolicitacaoServico> findByUsuarioIdEmAndamentoEmEsperaAguardando(@Param("id_usuario") Long idUsuario,
			@Param("emAndamento") StatusSolicitacaoServicoEnum emAndamento,
			@Param("emEspera") StatusSolicitacaoServicoEnum emEspera,
			@Param("aguardandoAprovacao") StatusSolicitacaoServicoEnum aguardandoAprovacao);

	/**
	 * Query de busca de usuáriio com status em andamento, em espera e aguardando
	 * 
	 * @param idUsuario
	 * @param status
	 * @param status2
	 * @param status3
	 * @return
	 */

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.usuario.id = u.id WHERE u.id = :id_usuario AND (t.status = :emAndamento OR t.status = :emEspera OR t.status = :aguardandoAprovacao OR t.status = :canceladoPrestador)")
	Optional<SolicitacaoServico> findByUsuarioIdEmAndamentoEmEsperaAguardandoCanceladoPrestador(
			@Param("id_usuario") Long idUsuario, @Param("emAndamento") StatusSolicitacaoServicoEnum emAndamento,
			@Param("emEspera") StatusSolicitacaoServicoEnum emEspera,
			@Param("aguardandoAprovacao") StatusSolicitacaoServicoEnum aguardandoAprovacao,
			@Param("canceladoPrestador") StatusSolicitacaoServicoEnum canceladoPrestador);

	/**
	 * Query de busca de usuário com status em andamento.
	 * 
	 * @param idUsuario
	 * @param status
	 * @param statusAguardando
	 * @return
	 */

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.usuario.id = u.id WHERE u.id = :id_usuario AND (t.status = :status OR t.status = :statusaguardando)")
	Optional<SolicitacaoServico> findByUsuarioIdEmAndamento(@Param("id_usuario") Long idUsuario,
			@Param("status") StatusSolicitacaoServicoEnum status,
			@Param("statusaguardando") StatusSolicitacaoServicoEnum statusAguardando);

	/**
	 * 
	 * Query de busca usuário com status em andamento e em espera.
	 * 
	 * @param idUsuario
	 * @param statusEmAndamento
	 * @param statusEmEspera
	 * @return
	 */

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.usuario.id = u.id WHERE u.id = :id_usuario AND (t.status = :statusEmAndamento OR t.status = :statusEmEspera)")
	Optional<SolicitacaoServico> findByUsuarioIdEmAndamentoEmEspera(@Param("id_usuario") Long idUsuario,
			@Param("statusEmAndamento") StatusSolicitacaoServicoEnum statusEmAndamento,
			@Param("statusEmEspera") StatusSolicitacaoServicoEnum statusEmEspera);

	/**
	 * Query de listagem de serviços com status em espera
	 * 
	 * @param idUsuario
	 * @return
	 */

	@Query("SELECT t FROM SolicitacaoServico as t WHERE t.servico.id = :id_servico AND t.status = 3")
	List<SolicitacaoServico> findByServicoIdEmEspera(@Param("id_servico") Long idUsuario);

	@Query("SELECT t FROM SolicitacaoServico as t INNER JOIN Usuario as u ON t.usuario.id = u.id WHERE u.id = :id_prestador")
	List<SolicitacaoServico> findByListaPrestadorDeServicoId(@Param("id_prestador") Long idPrestador);

	@Query("SELECT t FROM SolicitacaoServico as t WHERE t.id = :id_solicitacao AND (t.status = :status OR t.status = :status2 OR t.status = :status3)")
	Optional<SolicitacaoServico> findByIdEmAndamentoEmEsperaAguardando(@Param("id_solicitacao") Long idUsuario,
			@Param("status") StatusSolicitacaoServicoEnum status,
			@Param("status2") StatusSolicitacaoServicoEnum status2,
			@Param("status3") StatusSolicitacaoServicoEnum status3);

}
