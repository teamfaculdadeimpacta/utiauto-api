package br.com.utiauto.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.modelo.MensagemAjuda;

public interface MensagemAjudaRepository extends JpaRepository<MensagemAjuda, Long> {

	Optional<MensagemAjuda> findByUsuarioId(Long idUsuario);

	List<MensagemAjuda> findByTipoMensagem(TipoMensagemEnum ajuda);

	@Query("SELECT t FROM MensagemAjuda as t INNER JOIN Usuario as u ON t.usuario.id = u.id WHERE u.id = :id_usuario AND t.tipoMensagem = :tipoMensagem")
	List<MensagemAjuda> findByUsuarioIdETipoMensagem(@Param("id_usuario") Long idUsuario, @Param("tipoMensagem") TipoMensagemEnum tipoMensagem);
	
	
}
