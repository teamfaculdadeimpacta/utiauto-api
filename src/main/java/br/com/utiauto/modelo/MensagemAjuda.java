package br.com.utiauto.modelo;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.enums.TipoMensagemEnum;

@Entity
public class MensagemAjuda {

	public MensagemAjuda() {
	}

	public MensagemAjuda(String mensagem, TipoMensagemEnum tipoMensagem, StatusMensagemEnum statusMensagem,
			Usuario usuario) {
		this.mensagem = mensagem;
		this.tipoMensagem = tipoMensagem;
		this.statusMensagem = statusMensagem;
		this.usuario = usuario;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String mensagem;

	private TipoMensagemEnum tipoMensagem;

	private StatusMensagemEnum statusMensagem;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = false, referencedColumnName = "id")
	private Usuario usuario;

	private LocalDateTime dataCriacao = LocalDateTime.now();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoMensagemEnum getTipoMensagem() {
		return tipoMensagem;
	}

	public void setTipoMensagem(TipoMensagemEnum tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}

	public void setStatusMensagem(StatusMensagemEnum statusMensagem) {
		this.statusMensagem = statusMensagem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(dataCriacao, id, mensagem, statusMensagem, tipoMensagem, usuario);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MensagemAjuda)) {
			return false;
		}
		MensagemAjuda other = (MensagemAjuda) obj;
		return Objects.equals(dataCriacao, other.dataCriacao) && Objects.equals(id, other.id)
				&& Objects.equals(mensagem, other.mensagem) && statusMensagem == other.statusMensagem
				&& tipoMensagem == other.tipoMensagem && Objects.equals(usuario, other.usuario);
	}

}