package br.com.utiauto.modelo;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.enums.TipoMensagemEnum;

@Entity
public class MensagemContestacao {

	public MensagemContestacao() {
	}

	public MensagemContestacao(String titulo, String mensagem, TipoMensagemEnum tipoMensagem, StatusMensagemEnum statusMensagem,
			SolicitacaoServico solicitacaoServico) {
		this.titulo = titulo;
		this.mensagem = mensagem;
		this.tipoMensagem = tipoMensagem;
		this.statusMensagem = statusMensagem;
		this.solicitacaoServico = solicitacaoServico;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String titulo;

	private String mensagem;

	private TipoMensagemEnum tipoMensagem;

	private StatusMensagemEnum statusMensagem;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "solicitacao_id", nullable = false, referencedColumnName = "id")
	private SolicitacaoServico solicitacaoServico;



	private LocalDateTime dataCriacao = LocalDateTime.now();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoMensagemEnum getTipoMensagem() {
		return tipoMensagem;
	}

	public void setTipoMensagem(TipoMensagemEnum tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public SolicitacaoServico getSolicitacaoServico() {
		return solicitacaoServico;
	}

	public void setSolicitacaoServico(SolicitacaoServico solicitacaoServico) {
		this.solicitacaoServico = solicitacaoServico;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}

	public void setStatusMensagem(StatusMensagemEnum statusMensagem) {
		this.statusMensagem = statusMensagem;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(dataCriacao, id, mensagem, solicitacaoServico, statusMensagem, tipoMensagem, titulo);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MensagemContestacao)) {
			return false;
		}
		MensagemContestacao other = (MensagemContestacao) obj;
		return Objects.equals(dataCriacao, other.dataCriacao) && Objects.equals(id, other.id)
				&& Objects.equals(mensagem, other.mensagem)
				&& Objects.equals(solicitacaoServico, other.solicitacaoServico)
				&& statusMensagem == other.statusMensagem && tipoMensagem == other.tipoMensagem
				&& Objects.equals(titulo, other.titulo);
	}




}