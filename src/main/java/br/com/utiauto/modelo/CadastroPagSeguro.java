package br.com.utiauto.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class CadastroPagSeguro {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String emailPagSeguro;
	
	private String password;
	
	@OneToOne
	private Usuario usuario;
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public CadastroPagSeguro() {
		
	}

	public CadastroPagSeguro(String emailPagSeguro, String password, Usuario usuario) {
		this.emailPagSeguro = emailPagSeguro;
		this.password = password;
		this.usuario = usuario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the emailPagSeguro
	 */
	public String getEmailPagSeguro() {
		return emailPagSeguro;
	}

	/**
	 * @param emailPagSeguro the emailPagSeguro to set
	 */
	public void setEmailPagSeguro(String emailPagSeguro) {
		this.emailPagSeguro = emailPagSeguro;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String senha) {
		this.password = senha;
	}
	
	
	
	
}
