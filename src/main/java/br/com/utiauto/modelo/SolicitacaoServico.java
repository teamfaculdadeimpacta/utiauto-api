package br.com.utiauto.modelo;

import java.time.LocalDateTime;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.enums.TipoDePagamentoEnum;

@Entity
public class SolicitacaoServico {

	public SolicitacaoServico() {

	}

	public SolicitacaoServico(Servico servico, Usuario usuario, EnderecoSolicitacao enderecoSolicitacao,
			Automovel automovel, StatusSolicitacaoServicoEnum status, TipoDePagamentoEnum tipoDePagamento,
			String[] localizacaoFinal) {
		this.servico = servico;
		this.usuario = usuario;
		this.enderecoSolicitacao = enderecoSolicitacao;
		this.automovel = automovel;
		this.tipoDePagamento = tipoDePagamento;
		this.status = status;
		this.localizacaoFinal = localizacaoFinal;
	}

	@Column(unique = true)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servico_id", nullable = false, referencedColumnName = "id")
	private Servico servico;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = false, referencedColumnName = "id")
	private Usuario usuario;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prestador_id", nullable = true, referencedColumnName = "id")
	private Usuario prestadorDeServico;

	@JoinColumn(name = "enderecosolicitacao_id", nullable = false, referencedColumnName = "id")
	@OneToOne(fetch = FetchType.LAZY)
	private EnderecoSolicitacao enderecoSolicitacao;

	private String[] localizacaoInicial;

	private String[] localizacaoFinal;

	private String[] localizacaoAtual;

	@JoinColumn(name = "automovel_id", nullable = false, referencedColumnName = "id")
	@OneToOne(fetch = FetchType.LAZY)
	private Automovel automovel;

	private TipoDePagamentoEnum tipoDePagamento = TipoDePagamentoEnum.DINHEIRO;

	private StatusSolicitacaoServicoEnum status = StatusSolicitacaoServicoEnum.EMESPERA;

	private LocalDateTime dataCriacao = LocalDateTime.now();

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public StatusSolicitacaoServicoEnum getStatus() {
		return status;
	}

	public void setStatus(StatusSolicitacaoServicoEnum status) {
		this.status = status;
	}

	public Usuario getPrestadorDeServico() {
		return prestadorDeServico;
	}

	public void setPrestadorDeServico(Usuario prestadorDeServico) {
		this.prestadorDeServico = prestadorDeServico;
	}

	public TipoDePagamentoEnum getTipoDePagamento() {
		return tipoDePagamento;
	}

	public void setTipoDePagamento(TipoDePagamentoEnum tipoDePagamento) {
		this.tipoDePagamento = tipoDePagamento;
	}

	public EnderecoSolicitacao getEnderecoSolicitacao() {
		return enderecoSolicitacao;
	}

	public void setEnderecoSolicitacao(EnderecoSolicitacao enderecoSolicitacao) {
		this.enderecoSolicitacao = enderecoSolicitacao;
	}

	public String[] getLocalizacaoInicial() {
		return localizacaoInicial;
	}

	public void setLocalizacaoInicial(String[] localizacaoInicial) {
		this.localizacaoInicial = localizacaoInicial;
	}

	public String[] getLocalizacaoFinal() {
		return localizacaoFinal;
	}

	public void setLocalizacaoFinal(String[] localizacaoFinal) {
		this.localizacaoFinal = localizacaoFinal;
	}

	public String[] getLocalizacaoAtual() {
		return localizacaoAtual;
	}

	public void setLocalizacaoAtual(String[] localizacaoAtual) {
		this.localizacaoAtual = localizacaoAtual;
	}

	public Automovel getAutomovel() {
		return automovel;
	}

	public void setAutomovel(Automovel automovel) {
		this.automovel = automovel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((automovel == null) ? 0 : automovel.hashCode());
		result = prime * result + ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
		result = prime * result + ((enderecoSolicitacao == null) ? 0 : enderecoSolicitacao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Arrays.hashCode(localizacaoAtual);
		result = prime * result + Arrays.hashCode(localizacaoFinal);
		result = prime * result + Arrays.hashCode(localizacaoInicial);
		result = prime * result + ((prestadorDeServico == null) ? 0 : prestadorDeServico.hashCode());
		result = prime * result + ((servico == null) ? 0 : servico.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tipoDePagamento == null) ? 0 : tipoDePagamento.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitacaoServico other = (SolicitacaoServico) obj;
		if (automovel == null) {
			if (other.automovel != null)
				return false;
		} else if (!automovel.equals(other.automovel))
			return false;
		if (dataCriacao == null) {
			if (other.dataCriacao != null)
				return false;
		} else if (!dataCriacao.equals(other.dataCriacao))
			return false;
		if (enderecoSolicitacao == null) {
			if (other.enderecoSolicitacao != null)
				return false;
		} else if (!enderecoSolicitacao.equals(other.enderecoSolicitacao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (!Arrays.equals(localizacaoAtual, other.localizacaoAtual))
			return false;
		if (!Arrays.equals(localizacaoFinal, other.localizacaoFinal))
			return false;
		if (!Arrays.equals(localizacaoInicial, other.localizacaoInicial))
			return false;
		if (prestadorDeServico == null) {
			if (other.prestadorDeServico != null)
				return false;
		} else if (!prestadorDeServico.equals(other.prestadorDeServico))
			return false;
		if (servico == null) {
			if (other.servico != null)
				return false;
		} else if (!servico.equals(other.servico))
			return false;
		if (status != other.status)
			return false;
		if (tipoDePagamento != other.tipoDePagamento)
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

}
