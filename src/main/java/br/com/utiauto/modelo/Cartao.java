package br.com.utiauto.modelo;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.com.utiauto.enums.TipoCartaoEnum;

@Entity
public class Cartao {
	
	
	public Cartao() {
	}
	
	public Cartao(Long nrCartao, String dtVencimento, int codSeg, String pais, TipoCartaoEnum tpCartao, Usuario usuario) {
		this.nrCartao = nrCartao;
		this.dtVencimento = dtVencimento;
		this.codSeg = codSeg;
		this.pais = pais;
		this.usuario = usuario;
		this.tpCartao = tpCartao;
	}
	

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Long nrCartao;
	
	private String dtVencimento;
	
	private int codSeg;
	
	private String pais;
	
	@OneToOne
	private Usuario usuario;
	
	private TipoCartaoEnum tpCartao;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNrCartao() {
		return nrCartao;
	}

	public void setNrCartao(Long nrCartao) {
		this.nrCartao = nrCartao;
	}

	public String getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public int getCodSeg() {
		return codSeg;
	}

	public void setCodSeg(int codSeg) {
		this.codSeg = codSeg;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoCartaoEnum getTpCartao() {
		return tpCartao;
	}

	public void setTpCartao(TipoCartaoEnum tpCartao) {
		this.tpCartao = tpCartao;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(codSeg, dtVencimento, id, nrCartao, pais, tpCartao, usuario);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Cartao)) {
			return false;
		}
		Cartao other = (Cartao) obj;
		return codSeg == other.codSeg && Objects.equals(dtVencimento, other.dtVencimento)
				&& Objects.equals(id, other.id) && Objects.equals(nrCartao, other.nrCartao)
				&& Objects.equals(pais, other.pais) && tpCartao == other.tpCartao
				&& Objects.equals(usuario, other.usuario);
	}

	
	
	

}
