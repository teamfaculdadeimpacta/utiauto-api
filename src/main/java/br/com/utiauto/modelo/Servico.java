package br.com.utiauto.modelo;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Servico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nomeServico;
	private String iconeServico;
	private double valorServico;
	

	private LocalDateTime dataCriacao = LocalDateTime.now();

	
	public Servico() {
	}

	public Servico(String nomeServico, String iconeServico, double valorServico) {
		this.nomeServico = nomeServico;
		this.iconeServico = iconeServico;
		this.valorServico = valorServico;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getIconeServico() {
		return iconeServico;
	}

	public void setIconeServico(String iconeServico) {
		this.iconeServico = iconeServico;
	}
	
	
	public double getValorServico() {
		return valorServico;
	}

	public void setValorServico(double valorServico) {
		this.valorServico = valorServico;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dataCriacao, iconeServico, id, nomeServico, valorServico);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Servico)) {
			return false;
		}
		Servico other = (Servico) obj;
		return Objects.equals(dataCriacao, other.dataCriacao) && Objects.equals(iconeServico, other.iconeServico)
				&& Objects.equals(id, other.id) && Objects.equals(nomeServico, other.nomeServico)
				&& Double.doubleToLongBits(valorServico) == Double.doubleToLongBits(other.valorServico);
	}
	



}
