package br.com.utiauto.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.utiauto.modelo.Servico;
import br.com.utiauto.repository.ServicoRepository;
import br.com.utiauto.service.PrestadorDeServicoService;

public class PrestadorDeServicoServiceImpl implements PrestadorDeServicoService {

	
	@Override
	public List<Servico> verificaServicos(List<Long> lista, ServicoRepository servicoRepository) {
		List<Servico> servicos = new ArrayList<>();
		for (Long servico : lista) {
			Optional<Servico> findById = servicoRepository.findById(servico);
			if (findById.isPresent()) {
				servicos.add(findById.get());
			}
		}
		return servicos;
	}

}
