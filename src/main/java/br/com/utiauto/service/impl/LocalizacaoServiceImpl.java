package br.com.utiauto.service.impl;

import org.apache.log4j.Logger;

import br.com.utiauto.dto.localizacao.LocalizacaoDto;
import br.com.utiauto.form.localizacao.LocalizacaoForm;
import br.com.utiauto.service.LocalizacaoService;

public class LocalizacaoServiceImpl implements LocalizacaoService {

	private final Logger logger = Logger.getLogger(this.getClass());
	
	
	@Override
	public LocalizacaoDto setaLocalizacao(String[] pontosDeLocalizacao) {
		LocalizacaoDto localizacaoDto = null;
		try {
			localizacaoDto = incluiLocalizacao(pontosDeLocalizacao);
		} catch (Exception e) {
			logger.error("Error in " + this.getClass() + ": ", e);
			localizacaoDto = new LocalizacaoDto();
		}
		return localizacaoDto;
	}


	private LocalizacaoDto incluiLocalizacao(String[] pontosDeLocalizacao) {
		LocalizacaoForm localizacaoForm;
		if(pontosDeLocalizacao != null && pontosDeLocalizacao.length == 2) {
			localizacaoForm = new LocalizacaoForm(pontosDeLocalizacao[0], pontosDeLocalizacao[1]);
			return localizacaoForm.converter();
		}
		return new LocalizacaoDto();
	}

}
