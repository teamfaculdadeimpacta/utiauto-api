package br.com.utiauto.service;

import java.util.List;

import br.com.utiauto.modelo.Servico;
import br.com.utiauto.repository.ServicoRepository;

public interface PrestadorDeServicoService {
	
	List<Servico> verificaServicos(List<Long> lista, ServicoRepository servicoRepository);

}
