package br.com.utiauto.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;

public interface CrudControllerService<Entidade, Dto, DetalheDto, Form, AtualizaForm> {

	List<Dto> listar(Long id);

	ResponseEntity<Dto> cadastrar(@PathVariable Long id, @RequestBody @Valid Form form,
			UriComponentsBuilder uriBuilder);

	ResponseEntity<DetalheDto> detalhar(@PathVariable Long id);

	ResponseEntity<DetalheDto> atualizar(@PathVariable Long id, @RequestBody @Valid AtualizaForm form);

	ResponseEntity<Entidade> remover(@PathVariable Long id);

}
