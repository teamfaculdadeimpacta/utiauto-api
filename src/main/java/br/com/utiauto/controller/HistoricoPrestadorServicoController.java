package br.com.utiauto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.utiauto.dto.solicitacao.SolicitacaoServicoDto;
import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.repository.SolicitaServicoRepository;

@RestController
@RequestMapping("/historico-prestador")
public class HistoricoPrestadorServicoController {

	@Autowired
	private SolicitaServicoRepository solicitaServicoRepository;
	
	@CrossOrigin
	@GetMapping
	public List<SolicitacaoServicoDto> list(Long idPrestador, StatusSolicitacaoServicoEnum status){
		List<SolicitacaoServico> servicos;
		servicos = solicitaServicoRepository.findByIdPrestadorStatus(idPrestador, status);
		return SolicitacaoServicoDto.converter(servicos);
	}
	
	
	@CrossOrigin
	@GetMapping("/qtdDia")
	public int quantidadeDoDia(Long idPrestador) {
		List<SolicitacaoServico> servicos;
		servicos = solicitaServicoRepository.findByIdPrestadorDia(idPrestador, StatusSolicitacaoServicoEnum.APROVADO);
		return servicos.size();	
	}
	
	@CrossOrigin
	@GetMapping("/qtdSemana")
	public int quantidadeDaSemana(Long idPrestador) {
		List<SolicitacaoServico> servicos;
		servicos = solicitaServicoRepository.findByIdPrestadorSemana(idPrestador, StatusSolicitacaoServicoEnum.APROVADO);
		return servicos.size();	
	}
	
}
