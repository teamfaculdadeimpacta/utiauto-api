package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.mensagemcontestacao.AtualizacaoMensagemDto;
import br.com.utiauto.dto.mensagemcontestacao.DetalhesMensagemDto;
import br.com.utiauto.dto.mensagemcontestacao.MensagemDto;
import br.com.utiauto.form.mensagemcontestacao.AtualizaMensagemContestacaoForm;
import br.com.utiauto.form.mensagemcontestacao.MensagemContestacaoForm;
import br.com.utiauto.modelo.MensagemContestacao;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.repository.MensagemContestacaoRepository;
import br.com.utiauto.repository.SolicitaServicoRepository;

@RestController
@RequestMapping("mensagem")

public class MensagemContestacaoController {

	@Autowired
	private SolicitaServicoRepository solicitacaoServicoRepository;
	
	@Autowired
	private MensagemContestacaoRepository mensagemRepository;

	@CrossOrigin
	@GetMapping
	public List<MensagemDto> listar(Long id) {
		return MensagemDto.converter(mensagemRepository.findAll());
	}
	
	
	@CrossOrigin
	@PostMapping("/{idSolicitacao}")
	@Transactional
	public ResponseEntity<MensagemDto> cadastrar(@PathVariable Long idSolicitacao, @RequestBody MensagemContestacaoForm form, UriComponentsBuilder uriBuilder){
		Optional<SolicitacaoServico> solicitacaoServico = solicitacaoServicoRepository.findById(idSolicitacao);
		if(solicitacaoServico.isPresent()) {
				MensagemContestacao mensagem = form.converter(solicitacaoServico.get());
				mensagemRepository.save(mensagem);
				URI uri = uriBuilder.path("/mensagem/{id}").buildAndExpand(mensagem.getId()).toUri();
				return ResponseEntity.created(uri).body(new MensagemDto(mensagem));
		}
		return ResponseEntity.notFound().build();
	}
	
	
	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<AtualizacaoMensagemDto> atualizar(@PathVariable Long id, @RequestBody AtualizaMensagemContestacaoForm form){
		Optional<MensagemContestacao> optional = mensagemRepository.findBySolicitacaoServico_id(id);
		if(optional.isPresent()) {
			MensagemContestacao mensagem = form.atualizar(optional.get());
			return ResponseEntity.ok(new AtualizacaoMensagemDto(mensagem));
		}
		return ResponseEntity.notFound().build();
	}
	
	
	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesMensagemDto> detalhar(@PathVariable Long id){
		Optional<MensagemContestacao> optional = mensagemRepository.findBySolicitacaoServico_id(id);
		if(optional.isPresent()) {
			return ResponseEntity.ok(new DetalhesMensagemDto(optional.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@Transactional
	@DeleteMapping("/{id}")
	public ResponseEntity<MensagemContestacao> remover(Long id) {
		Optional<MensagemContestacao> optional = mensagemRepository.findById(id);
		if (optional.isPresent()) {
			mensagemRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}



	
	
	
	
}
