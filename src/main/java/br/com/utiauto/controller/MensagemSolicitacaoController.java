package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.mensagemsolicitacao.DetalheMensagemSolicitacaoDto;
import br.com.utiauto.dto.mensagemsolicitacao.MensagemSolicitacaoDto;
import br.com.utiauto.form.mensagemsolicitacao.MensagemSolicitacaoForm;
import br.com.utiauto.modelo.MensagemSolicitacao;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.MensagemSolicitacaoRepository;
import br.com.utiauto.repository.SolicitaServicoRepository;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("mensagem-solicitacao")
public class MensagemSolicitacaoController {

	@Autowired
	private MensagemSolicitacaoRepository mensagemSolicitacaoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private SolicitaServicoRepository solicitaServicoRepository;

	
	@CrossOrigin
	@GetMapping
	@Cacheable(value = "listaDeMensagem")
	public List<MensagemSolicitacaoDto> lista(Long id) {
		List<MensagemSolicitacao> mensagemSolicitacao = null;
		if(id == null) {
			mensagemSolicitacao = mensagemSolicitacaoRepository.findAll();
		} else { 
			mensagemSolicitacao = mensagemSolicitacaoRepository.findBySolicitacaoServicoId(id);
		}
		return MensagemSolicitacaoDto.converter(mensagemSolicitacao);
	}

	@CrossOrigin
	@PostMapping("/solicitacao/{idSolicitacao}/usuario/{idUsuario}")
	@Transactional
	@CacheEvict(value = "listaDeMensagem", allEntries = true)
	public ResponseEntity<MensagemSolicitacaoDto> cadastrarMensagemUsuario(@PathVariable Long idSolicitacao,
			@PathVariable Long idUsuario, @RequestBody MensagemSolicitacaoForm form, UriComponentsBuilder uriBuilder) {
		Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
		Optional<SolicitacaoServico> solicitacao = solicitaServicoRepository.findById(idSolicitacao);
		if (usuario.isPresent() && solicitacao.isPresent()) {
			
			MensagemSolicitacao mensagemSolicitacao = form.converter(usuario.get(), solicitacao.get());
			mensagemSolicitacaoRepository.save(mensagemSolicitacao);
			URI uri = uriBuilder.path("/mensagem-solicitacao/{id}").buildAndExpand(mensagemSolicitacao.getId()).toUri();
			return ResponseEntity.created(uri).body(new MensagemSolicitacaoDto(mensagemSolicitacao));
		}
		return ResponseEntity.notFound().build();
	}
	
	
	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<DetalheMensagemSolicitacaoDto> detalhar(@PathVariable Long id){
		Optional<MensagemSolicitacao> optional = mensagemSolicitacaoRepository.findById(id);
		if(optional.isPresent()) {
			return ResponseEntity.ok(new DetalheMensagemSolicitacaoDto(optional.get()));
		}
		return ResponseEntity.notFound().build();
	}
	

}
