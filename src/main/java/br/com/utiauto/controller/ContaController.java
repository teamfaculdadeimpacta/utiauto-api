package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.conta.ContaDto;
import br.com.utiauto.dto.conta.DetalhesContaDto;
import br.com.utiauto.form.conta.AtualizaContaForm;
import br.com.utiauto.form.conta.ContaForm;
import br.com.utiauto.modelo.Conta;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.ContaRepository;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("/conta")
public class ContaController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private ContaRepository contaRepository;

	@CrossOrigin
	@GetMapping
	public List<ContaDto> lista() {
		List<Conta> conta;
		conta = contaRepository.findAll();
		return ContaDto.converter(conta);
	}

	@CrossOrigin
	@PostMapping
	@Transactional
	public ResponseEntity<ContaDto> cadastrarEnderecoUsuario(@RequestBody @Valid ContaForm form,
			UriComponentsBuilder uriBuilder) {
		Optional<Usuario> prestador = usuarioRepository.findById(form.getIdPrestador());
		if (prestador.isPresent()) {
			Conta conta = form.converter(prestador.get());
			contaRepository.save(conta);
			URI uri = uriBuilder.path("/conta/{id}").buildAndExpand(conta.getId()).toUri();
			return ResponseEntity.created(uri).body(new ContaDto(conta));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesContaDto> detalhar(@PathVariable Long id) {
		Optional<Conta> conta = contaRepository.findByPrestadorId(id);
		if (conta.isPresent()) {
			return ResponseEntity.ok(new DetalhesContaDto(conta.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<DetalhesContaDto> atualizar(@PathVariable Long id, @RequestBody AtualizaContaForm form) {
		Optional<Conta> optional = contaRepository.findByPrestadorId(id);
		if (optional.isPresent()) {
			Conta conta = form.atualizar(optional.get().getId(), contaRepository);
			return ResponseEntity.ok(new DetalhesContaDto(conta));
		}
		return ResponseEntity.notFound().build();
	}

}
