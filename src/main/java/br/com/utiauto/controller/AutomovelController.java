package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.automovel.AutomovelDto;
import br.com.utiauto.dto.automovel.DetalhesDoAutomovelDto;
import br.com.utiauto.form.automovel.AtualizacaoAutomovelForm;
import br.com.utiauto.form.automovel.AutomovelForm;
import br.com.utiauto.modelo.Automovel;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.AutomovelRepository;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("/automovel")
public class AutomovelController {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	
	@Autowired
	private AutomovelRepository carroRepository;
	
	
	@CrossOrigin
	@GetMapping
	public List<AutomovelDto> carro(Long id) {
		List<Automovel> carro = carroRepository.findAll();
		return AutomovelDto.converter(carro);
	}
	
	@CrossOrigin
	@PostMapping
	@Transactional
	public ResponseEntity<AutomovelDto> cadastrar(@RequestBody @Valid AutomovelForm form, UriComponentsBuilder uriBuilder) {
		Optional<Usuario> usuario = usuarioRepository.findById(form.getIdUsuario());
		if(usuario.isPresent() && usuario.get().getAutomovel() == null) {
			Automovel carro = form.converter(usuario.get());
			carroRepository.save(carro);
			URI uri = uriBuilder.path("/carro/{id}").buildAndExpand(carro.getId()).toUri();
			return ResponseEntity.created(uri).body(new AutomovelDto(carro));
		}
		return ResponseEntity.notFound().build();
	}
	
	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesDoAutomovelDto> detalhar(@PathVariable Long id) {
		Optional<Automovel> carro = carroRepository.findByUsuarioId(id);
		if (carro.isPresent()) {
			return ResponseEntity.ok(new DetalhesDoAutomovelDto(carro.get()));
		}
		return ResponseEntity.notFound().build();		
	}
	
	@CrossOrigin
	@PutMapping("/{idUsuario}")
	@Transactional
	public ResponseEntity<AutomovelDto> atualizar(@PathVariable Long idUsuario, @RequestBody @Valid AtualizacaoAutomovelForm form){
		Optional<Automovel> optional = carroRepository.findByUsuarioId(idUsuario);
		if (optional.isPresent()) {
			Automovel carro = form.atualizar(optional.get());
			return ResponseEntity.ok(new AutomovelDto(carro));
		}
		return ResponseEntity.notFound().build();	
	}
	
	@CrossOrigin
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Automovel> remover(@PathVariable Long id){
		Optional<Automovel> optional = carroRepository.findById(id);
		if (optional.isPresent()) {
			carroRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}		
		return ResponseEntity.notFound().build();	

	}
	
	
}
