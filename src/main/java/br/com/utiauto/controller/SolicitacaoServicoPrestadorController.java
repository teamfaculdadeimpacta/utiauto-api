package br.com.utiauto.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.utiauto.dto.solicitacao.DetalhesSolicitacaoServicoDto;
import br.com.utiauto.dto.solicitacao.SolicitacaoServicoDto;
import br.com.utiauto.dto.solicitacao.SolicitacaoServicoSolicitadoDto;
import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.form.solicitacao.AtualizacaoSolicitacaoServicoPrestadorForm;
import br.com.utiauto.form.solicitacao.AtualizacaoSolicitacaoServicoPrestadorLocalizacaoForm;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.PrestadorRepository;
import br.com.utiauto.repository.SolicitaServicoRepository;

@RestController
@RequestMapping("/solicitacao-servico-prestador")
public class SolicitacaoServicoPrestadorController {

	@Autowired
	private PrestadorRepository prestadorRepository;

	@Autowired
	private SolicitaServicoRepository solicitaServicoRepository;

	@CrossOrigin
	@GetMapping
	public List<SolicitacaoServicoDto> list(Long idServico) {
		List<SolicitacaoServico> servicos;
		if (idServico == null) {
			servicos = solicitaServicoRepository.findAll();
		} else {
			servicos = solicitaServicoRepository.findByServicoId(idServico);
		}
		return SolicitacaoServicoDto.converter(servicos);
	}

	@CrossOrigin
	@GetMapping("/servico-em-espera")
	public List<SolicitacaoServicoDto> listEmEspera(Long idServico) {
		List<SolicitacaoServico> servicos;
		servicos = solicitaServicoRepository.findByServicoIdEmEspera(idServico);
		return SolicitacaoServicoDto.converter(servicos);
	}

	@CrossOrigin
	@GetMapping("/servico-em-andamento/prestador/{idPrestador}")
	public ResponseEntity<DetalhesSolicitacaoServicoDto> detalharPrestador(@PathVariable Long idPrestador) {
		Optional<SolicitacaoServico> solicitaoServico = solicitaServicoRepository
				.findByPrestadorDeServicoIdEmAndamento(idPrestador, StatusSolicitacaoServicoEnum.EMANDAMENTO);
		if (solicitaoServico.isPresent()) {
			return ResponseEntity.ok(new DetalhesSolicitacaoServicoDto(solicitaoServico.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@Transactional
	@PutMapping("/atualiza-solicitacao/{idSolicitacao}")
	public ResponseEntity<SolicitacaoServicoSolicitadoDto> atualizar(@PathVariable Long idSolicitacao,
			@RequestBody @Valid AtualizacaoSolicitacaoServicoPrestadorForm form) {
		Optional<SolicitacaoServico> solicitacaoServico = solicitaServicoRepository.findById(idSolicitacao);
		Optional<Usuario> prestadorDeServico = prestadorRepository.findByIdEPerfil(form.getIdPrestador());
		if (solicitacaoServico.isPresent() && prestadorDeServico.isPresent()) {
			SolicitacaoServico solicitacao = form.atualizar(solicitacaoServico.get(), prestadorDeServico.get());
			return ResponseEntity.ok(new SolicitacaoServicoSolicitadoDto(solicitacao));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@Transactional
	@PutMapping("/atualiza-solicitacao/localizacao/{idPrestador}")
	public ResponseEntity<SolicitacaoServicoSolicitadoDto> atualizarLocalizacao(@PathVariable Long idPrestador,
			@RequestBody @Valid AtualizacaoSolicitacaoServicoPrestadorLocalizacaoForm form) {
		Optional<SolicitacaoServico> solicitacaoServico = solicitaServicoRepository
				.findByPrestadorDeServicoIdEmAndamento(idPrestador, StatusSolicitacaoServicoEnum.EMANDAMENTO);
		if (solicitacaoServico.isPresent()) {
			SolicitacaoServico solicitacao = form.atualizar(solicitacaoServico.get());
			return ResponseEntity.ok(new SolicitacaoServicoSolicitadoDto(solicitacao));
		}
		return ResponseEntity.notFound().build();
	}
}
