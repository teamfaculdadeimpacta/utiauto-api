package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.constants.Constantes;
import br.com.utiauto.dto.prestador.DetalhePrestadorDeServicoDto;
import br.com.utiauto.dto.prestador.PrestadorDeServicoDto;
import br.com.utiauto.form.prestador.AtualizacaoPrestadorDeServicoForm;
import br.com.utiauto.form.prestador.PrestadorDeServicoForm;
import br.com.utiauto.modelo.Perfil;
import br.com.utiauto.modelo.Servico;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.PerfilRepository;
import br.com.utiauto.repository.ServicoRepository;
import br.com.utiauto.repository.UsuarioRepository;
import br.com.utiauto.service.PrestadorDeServicoService;
import br.com.utiauto.service.impl.PrestadorDeServicoServiceImpl;

@RestController
@RequestMapping("/prestadorservico")
public class PrestadorDeServicoController {



	@Autowired
	private ServicoRepository servicoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private PerfilRepository perfilRepository;

	@CrossOrigin
	@GetMapping
	public List<PrestadorDeServicoDto> lista(Long idServico) {
		List<Usuario> prestador = null;
		if (idServico == null) {
			prestador = usuarioRepository.findAll();
		} else {
			prestador = usuarioRepository.findByPerfilServicoId(idServico);
		}
		return PrestadorDeServicoDto.converter(prestador);
	}

	@CrossOrigin
	@PostMapping
	@Transactional
	public ResponseEntity<PrestadorDeServicoDto> cadastrar(@RequestBody @Valid PrestadorDeServicoForm form,
			UriComponentsBuilder uriBuilder) {
		PrestadorDeServicoService prestadorDeServicoService = new PrestadorDeServicoServiceImpl();
		List<Long> lista = form.getServicos();
		Optional<Perfil> perfil = perfilRepository.findById(Constantes.TIPO_PRESTADOR);
		List<Servico> servicos = prestadorDeServicoService.verificaServicos(lista, servicoRepository);

		if (!servicos.isEmpty() && perfil.isPresent()) {
			Usuario prestador = form.converter(perfil.get(), servicos);
			usuarioRepository.save(prestador);
			URI uri = uriBuilder.path("/prestadorservico/{id}").buildAndExpand(prestador.getId()).toUri();
			return ResponseEntity.created(uri).body(new PrestadorDeServicoDto(prestador));
		}
		return ResponseEntity.notFound().build();

	}

	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<DetalhePrestadorDeServicoDto> atualizar(@PathVariable Long id,
			@RequestBody @Valid AtualizacaoPrestadorDeServicoForm form) {
		PrestadorDeServicoService prestadorDeServicoService = new PrestadorDeServicoServiceImpl();
		Optional<Usuario> optional = usuarioRepository.findById(id);
		List<Long> lista = form.getServicos();
		Optional<Perfil> perfil = perfilRepository.findById(Constantes.TIPO_PRESTADOR);
		List<Servico> servicos = prestadorDeServicoService.verificaServicos(lista, servicoRepository);
		if (!servicos.isEmpty() && optional.isPresent()) {
			Usuario prestador = form.atualizar(id, usuarioRepository, perfil.get(), servicos);
			return ResponseEntity.ok(new DetalhePrestadorDeServicoDto(prestador));
		}
		return ResponseEntity.notFound().build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<PrestadorDeServicoDto> detalhar(@PathVariable Long id) {
		Optional<Usuario> prestador = usuarioRepository.findById(id);
		if (prestador.isPresent()) {
			return ResponseEntity.ok(new PrestadorDeServicoDto(prestador.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Usuario> remover(@PathVariable Long id) {
		Optional<Usuario> optional = usuarioRepository.findById(id);
		if (optional.isPresent()) {
			usuarioRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}

}
