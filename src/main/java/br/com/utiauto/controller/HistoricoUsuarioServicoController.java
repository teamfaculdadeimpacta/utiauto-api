package br.com.utiauto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.utiauto.dto.solicitacao.SolicitacaoServicoDto;
import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.repository.SolicitaServicoRepository;

@RestController
@RequestMapping("/historico")
public class HistoricoUsuarioServicoController {

	
	@Autowired
	private SolicitaServicoRepository solicitacaoServicoRepository;
	
	
	@CrossOrigin
	@GetMapping
	public List<SolicitacaoServicoDto> list(Long idUsuario) {
		List<SolicitacaoServico> servicos;
		servicos = solicitacaoServicoRepository.findByIdStatus(idUsuario, StatusSolicitacaoServicoEnum.APROVADO);
		return SolicitacaoServicoDto.converter(servicos);
	}
	
}
