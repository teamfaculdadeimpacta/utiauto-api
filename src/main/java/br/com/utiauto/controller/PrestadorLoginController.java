package br.com.utiauto.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.utiauto.dto.prestador.PrestadorDeServicoDto;
import br.com.utiauto.form.login.LoginForm;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("/prestadorLogin")
public class PrestadorLoginController {

	@Autowired
	UsuarioRepository prestadorDeServicoRepository;

	@CrossOrigin
	@PostMapping
	public ResponseEntity<PrestadorDeServicoDto> login(@RequestBody @Valid LoginForm loginForm) {
		Optional<Usuario> optional = prestadorDeServicoRepository.findByEmailSenhaEPerfil(loginForm.getEmail(),
				loginForm.getSenha(), 2L);
		if (optional.isPresent()) {
			return ResponseEntity.ok(new PrestadorDeServicoDto(optional.get()));
		}
		return ResponseEntity.notFound().build();
	}

}
