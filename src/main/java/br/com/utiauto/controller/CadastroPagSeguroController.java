package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.pagseguro.CadastroPagSeguroDto;
import br.com.utiauto.dto.pagseguro.DetalhesCadastroPagSeguroDto;
import br.com.utiauto.form.pagseguro.AtualizacaoCadastroPagSeguroForm;
import br.com.utiauto.form.pagseguro.CadastroPagSeguroForm;
import br.com.utiauto.modelo.CadastroPagSeguro;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.CadastroPagSeguroRepository;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("/cadastro-pagseguro")
public class CadastroPagSeguroController {



	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private CadastroPagSeguroRepository cadastroPagSeguroRepository;

	@CrossOrigin
	@GetMapping
	public List<CadastroPagSeguroDto> lista() {
		List<CadastroPagSeguro> cadastroPagSeguro = cadastroPagSeguroRepository.findAll();
		return CadastroPagSeguroDto.converter(cadastroPagSeguro);
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<CadastroPagSeguroDto> cadastrar(@RequestBody CadastroPagSeguroForm cadastroPagSeguroForm,
			UriComponentsBuilder uriBuilder) {
		Optional<Usuario> usuario = usuarioRepository.findById(cadastroPagSeguroForm.getIdUsuario());
		if(usuario.isPresent()) {
			CadastroPagSeguro cadastroPagSeguro = cadastroPagSeguroForm.converter(usuario.get());
			cadastroPagSeguroRepository.save(cadastroPagSeguro);
			URI uri = uriBuilder.path("/cadastro-pagseguro/{id}").buildAndExpand(cadastroPagSeguro.getId()).toUri();
			return ResponseEntity.created(uri).body(new CadastroPagSeguroDto(cadastroPagSeguro));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesCadastroPagSeguroDto> detalhar(@PathVariable Long id) {
		Optional<CadastroPagSeguro> pagSeguro = cadastroPagSeguroRepository.findByUsuarioId(id);
		if (pagSeguro.isPresent()) {
			return ResponseEntity.ok(new DetalhesCadastroPagSeguroDto(pagSeguro.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<CadastroPagSeguroDto> atualizar(@PathVariable Long id,
			@RequestBody @Valid AtualizacaoCadastroPagSeguroForm form) {
		Optional<CadastroPagSeguro> pagSeguro = cadastroPagSeguroRepository.findByUsuarioId(id);
		if (pagSeguro.isPresent()) {
			CadastroPagSeguro cadastroPagSeguro = form.atualizar(pagSeguro.get().getId(), cadastroPagSeguroRepository);
			return ResponseEntity.ok(new CadastroPagSeguroDto(cadastroPagSeguro));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@Transactional
	@DeleteMapping("/{id}")
	public ResponseEntity<CadastroPagSeguro> remover(@PathVariable Long id) {
		Optional<CadastroPagSeguro> pagSeguro = cadastroPagSeguroRepository.findById(id);
		if (pagSeguro.isPresent()) {
			cadastroPagSeguroRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}

}
