package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.usuario.DetalhesDoUsuarioDto;
import br.com.utiauto.dto.usuario.UsuarioDto;
import br.com.utiauto.form.usuario.AtualizacaoUsuarioForm;
import br.com.utiauto.form.usuario.AtualizacaoUsuarioParaPrestadorForm;
import br.com.utiauto.form.usuario.UsuarioForm;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.PerfilRepository;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	
	@Autowired
	private PerfilRepository perfilRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@CrossOrigin
	@GetMapping
	@Cacheable(value = "listaDeUsuarios")
	public List<UsuarioDto> lista(String nome) {	
		List<Usuario> usuario;
		if(nome == null) {
			usuario = usuarioRepository.findAll(); 
		} else {
			usuario = usuarioRepository.findByNome(nome);
		}
		return UsuarioDto.converter(usuario);
	}
	
	@CrossOrigin
	@PostMapping
	@Transactional
	@CacheEvict(value = "listaDeUsuarios", allEntries = true)
	public ResponseEntity<UsuarioDto> cadastrar(@RequestBody @Valid UsuarioForm form, UriComponentsBuilder uriBuilder) {
		Optional<Usuario> findByEmail = usuarioRepository.findByEmail(form.getEmail());
		if(!findByEmail.isPresent()) {
			Usuario usuario = form.converter(perfilRepository);
			usuarioRepository.save(usuario);
			URI uri = uriBuilder.path("/usuario/{id}").buildAndExpand(usuario.getId()).toUri();
			return ResponseEntity.created(uri).body(new UsuarioDto(usuario));
		}
		return ResponseEntity.notFound().build();
	}
	
	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesDoUsuarioDto> detalhar(@PathVariable Long id) {
		Optional<Usuario> usuario= usuarioRepository.findById(id);
		if (usuario.isPresent()) {
			return ResponseEntity.ok(new DetalhesDoUsuarioDto(usuario.get()));
		}
		return ResponseEntity.notFound().build();		
	}
	
	
	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<UsuarioDto> atualizar(@PathVariable Long id, @RequestBody @Valid AtualizacaoUsuarioForm form) throws Exception{
		Optional<Usuario> optional = usuarioRepository.findById(id);
		if (optional.isPresent()) {
			Usuario usuario = form.atualizar(id, usuarioRepository);
			return ResponseEntity.ok(new UsuarioDto(usuario));
		}
		return ResponseEntity.notFound().build();	
	}
	
	@CrossOrigin
	@PutMapping("/atualiza-para-prestador/usuario/{id}")
	@Transactional
	public ResponseEntity<UsuarioDto> atualizaParaPrestador(@PathVariable Long id) throws Exception{
		AtualizacaoUsuarioParaPrestadorForm form = new AtualizacaoUsuarioParaPrestadorForm();
		Optional<Usuario> optional = usuarioRepository.findById(id);
		if (optional.isPresent() && optional.get().getPerfil().getId() == 1) {
			Usuario usuario = form.atualizar(optional.get(), perfilRepository);
			return ResponseEntity.ok(new UsuarioDto(usuario));
		}
		return ResponseEntity.notFound().build();	
	}
	
	
	@CrossOrigin
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Usuario> remover(@PathVariable Long id){
		Optional<Usuario> optional = usuarioRepository.findById(id);
		if (optional.isPresent()) {
			usuarioRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}		
		return ResponseEntity.notFound().build();	
	}
	
	

	
	
}
