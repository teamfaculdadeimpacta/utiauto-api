package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.endereco.DetalhesDoEnderecoDto;
import br.com.utiauto.dto.endereco.EnderecoDto;
import br.com.utiauto.form.endereco.AtualizacaoEnderecoForm;
import br.com.utiauto.form.endereco.EnderecoForm;
import br.com.utiauto.modelo.Endereco;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.EnderecoRepository;
import br.com.utiauto.repository.UsuarioRepository;
import br.com.utiauto.service.CrudControllerService;

@RestController
@RequestMapping("/endereco")
public class EnderecoController implements CrudControllerService<Endereco, EnderecoDto, DetalhesDoEnderecoDto, EnderecoForm, AtualizacaoEnderecoForm>{

	@Autowired
	private EnderecoRepository enderecoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@CrossOrigin
	@GetMapping
	public List<EnderecoDto> listar(Long id) {
		List<Endereco> endereco;
		endereco = enderecoRepository.findAll();
		return EnderecoDto.converter(endereco);
	}

	@CrossOrigin
	@PostMapping("/{id}")
	@Transactional
	public ResponseEntity<EnderecoDto> cadastrar(@PathVariable Long id,
			@RequestBody @Valid EnderecoForm form, UriComponentsBuilder uriBuilder) {
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		if (!usuario.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Endereco endereco = form.converter(usuario.get());
		enderecoRepository.save(endereco);
		URI uri = uriBuilder.path("/endereco/{id}").buildAndExpand(endereco.getId()).toUri();

		return ResponseEntity.created(uri).body(new EnderecoDto(endereco));
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesDoEnderecoDto> detalhar(@PathVariable Long id) {
		Optional<Endereco> endereco = enderecoRepository.findByUsuarioId(id);
		if (endereco.isPresent()) {
			return ResponseEntity.ok(new DetalhesDoEnderecoDto(endereco.get()));
		}
		return ResponseEntity.notFound().build();
	}


	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<DetalhesDoEnderecoDto> atualizar(@PathVariable Long id,
			@RequestBody @Valid AtualizacaoEnderecoForm form) {

		Optional<Endereco> enderecoOpcional = enderecoRepository.findByUsuarioId(id);
		if (enderecoOpcional.isPresent()) {
			Endereco endereco = form.atualizar(enderecoOpcional.get());
			return ResponseEntity.ok(new DetalhesDoEnderecoDto(endereco));
		}
		return ResponseEntity.notFound().build();
	}


	@CrossOrigin
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Endereco> remover(@PathVariable Long id) {
		Optional<Endereco> enderecoOpcional = enderecoRepository.findByUsuarioId(id);
		if (enderecoOpcional.isPresent()) {
			enderecoRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}



}
