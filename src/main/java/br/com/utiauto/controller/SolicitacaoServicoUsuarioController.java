package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.solicitacao.DetalhesSolicitacaoServicoDto;
import br.com.utiauto.dto.solicitacao.SolicitacaoServicoDto;
import br.com.utiauto.dto.solicitacao.SolicitacaoServicoSolicitadoDto;
import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.form.enderecosolicitacao.EnderecoSolicitacaoForm;
import br.com.utiauto.form.solicitacao.AtualizacaoSolicitacaoServicoUsuarioForm;
import br.com.utiauto.form.solicitacao.SolicitacaoServicoForm;
import br.com.utiauto.modelo.Automovel;
import br.com.utiauto.modelo.EnderecoSolicitacao;
import br.com.utiauto.modelo.Servico;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.AutomovelRepository;
import br.com.utiauto.repository.EnderecoSolicitacaoRepository;
import br.com.utiauto.repository.ServicoRepository;
import br.com.utiauto.repository.SolicitaServicoRepository;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("/solicitacao-servico-usuario")
public class SolicitacaoServicoUsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private ServicoRepository servicoRepository;

	@Autowired
	private SolicitaServicoRepository solicitaServicoRepository;

	@Autowired
	private EnderecoSolicitacaoRepository enderecoSolicitacaoRepository;

	@Autowired
	private AutomovelRepository automovelRepository;

	@CrossOrigin
	@GetMapping
	public List<SolicitacaoServicoDto> list(Long idServico) {
		List<SolicitacaoServico> servicos;
		if (idServico == null) {
			servicos = solicitaServicoRepository.findAll();
		} else {
			servicos = solicitaServicoRepository.findByServicoId(idServico);
		}
		return SolicitacaoServicoDto.converter(servicos);
	}

	@CrossOrigin
	@GetMapping("/detalhar-solicicitacao/solicitacao/{id}")
	public ResponseEntity<DetalhesSolicitacaoServicoDto> detalharPorSolicitacao(@PathVariable Long id) {
		Optional<SolicitacaoServico> solicitaoServico = solicitaServicoRepository.findById(id);
		if (solicitaoServico.isPresent()) {
			return ResponseEntity.ok(new DetalhesSolicitacaoServicoDto(solicitaoServico.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@GetMapping("/detalhar-solicicitacao/usuario/{id}")
	public ResponseEntity<DetalhesSolicitacaoServicoDto> detalharPorUsuario(@PathVariable Long id) {
		Optional<SolicitacaoServico> solicitaoServico = solicitaServicoRepository.findByUsuarioId(id);
		if (solicitaoServico.isPresent()) {
			return ResponseEntity.ok(new DetalhesSolicitacaoServicoDto(solicitaoServico.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@GetMapping("/servico-em-andamento/usuario/{id}")
	public ResponseEntity<DetalhesSolicitacaoServicoDto> detalharUsuarioEmAndamentoEmEsperaAguardando(
			@PathVariable Long id) {
		Optional<SolicitacaoServico> solicitaoServico = solicitaServicoRepository
				.findByUsuarioIdEmAndamentoEmEsperaAguardandoCanceladoPrestador(id,
						StatusSolicitacaoServicoEnum.EMANDAMENTO, StatusSolicitacaoServicoEnum.EMESPERA,
						StatusSolicitacaoServicoEnum.AGUARDANDO_APROVACAO,
						StatusSolicitacaoServicoEnum.CANCELADO_PRESTADOR);
		if (solicitaoServico.isPresent()) {
			return ResponseEntity.ok(new DetalhesSolicitacaoServicoDto(solicitaoServico.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@PostMapping
	@Transactional
	public ResponseEntity<SolicitacaoServicoDto> solicitar(@RequestBody @Valid SolicitacaoServicoForm form,
			UriComponentsBuilder uriBuilder) {
		Optional<Servico> servico = servicoRepository.findById(form.getIdServico());
		Optional<Usuario> usuario = usuarioRepository.findById(form.getIdUsuario());
		Optional<Automovel> automovel = automovelRepository.findByUsuarioId(form.getIdUsuario());
		Optional<SolicitacaoServico> solicitacaoServico = solicitaServicoRepository
				.findByUsuarioIdEmAndamentoEmEsperaAguardandoCanceladoPrestador(form.getIdUsuario(),
						StatusSolicitacaoServicoEnum.EMANDAMENTO, StatusSolicitacaoServicoEnum.EMESPERA,
						StatusSolicitacaoServicoEnum.AGUARDANDO_APROVACAO,
						StatusSolicitacaoServicoEnum.CANCELADO_PRESTADOR);

		if (servico.isPresent() && usuario.isPresent() && automovel.isPresent() && !solicitacaoServico.isPresent()) {
			EnderecoSolicitacao endereco = new EnderecoSolicitacaoForm().converter(form.getEnderecoSolicitacao());
			EnderecoSolicitacao enderecoSolicitacao = enderecoSolicitacaoRepository.save(endereco);
			SolicitacaoServico solicitaoServico = form.converter(servico.get(), usuario.get(), automovel.get(),
					enderecoSolicitacao);
			solicitaServicoRepository.save(solicitaoServico);
			URI uri = uriBuilder.path("/solicitacao-de-servico/{id}").buildAndExpand(solicitaoServico.getId()).toUri();
			return ResponseEntity.created(uri).body(new SolicitacaoServicoDto(solicitaoServico));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@PutMapping("/atualiza-solicitacao/usuario/{id}")
	@Transactional
	public ResponseEntity<SolicitacaoServicoSolicitadoDto> atualizarSolicitacaoUsuario(@PathVariable Long id,
			@RequestBody @Valid AtualizacaoSolicitacaoServicoUsuarioForm form) {
		Optional<SolicitacaoServico> solicitacaoServico = solicitaServicoRepository
				.findByUsuarioIdEmAndamentoEmEsperaAguardandoCanceladoPrestador(id,
						StatusSolicitacaoServicoEnum.EMANDAMENTO, StatusSolicitacaoServicoEnum.EMESPERA,
						StatusSolicitacaoServicoEnum.AGUARDANDO_APROVACAO,
						StatusSolicitacaoServicoEnum.CANCELADO_PRESTADOR);
		if (solicitacaoServico.isPresent()) {
			SolicitacaoServico solicitacao = form.atualizar(solicitacaoServico.get());
			return ResponseEntity.ok(new SolicitacaoServicoSolicitadoDto(solicitacao));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<SolicitacaoServico> delete(@PathVariable Long id) {
		Optional<SolicitacaoServico> optional = solicitaServicoRepository.findById(id);
		if (optional.isPresent()) {
			solicitaServicoRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}

}
