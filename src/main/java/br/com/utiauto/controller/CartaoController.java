package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.cartao.CartaoDto;
import br.com.utiauto.dto.cartao.DetalhesCartaoDto;
import br.com.utiauto.form.cartao.AtualizaCartaoForm;
import br.com.utiauto.form.cartao.CartaoForm;
import br.com.utiauto.modelo.Cartao;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.CartaoRepository;
import br.com.utiauto.repository.UsuarioRepository;


@RequestMapping("/cartao")
@RestController
public class CartaoController {
	
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private CartaoRepository cartaoRepository;
	
	
	@CrossOrigin
	@GetMapping
	public List<CartaoDto> lista(Long idUsuario){
		List<Cartao> cartoes;
		cartoes = cartaoRepository.findByUsuarioId(idUsuario);
		return CartaoDto.converter(cartoes);
	}
	
	
	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesCartaoDto> detalhar(@PathVariable Long id){
		Optional<Cartao> optional = cartaoRepository.findById(id);
		if(optional.isPresent()){
			return ResponseEntity.ok(new DetalhesCartaoDto(optional.get()));
		}
		return ResponseEntity.notFound().build();
	}
	
	
	@CrossOrigin
	@PostMapping("/{idUsuario}")
	@Transactional
	public ResponseEntity<CartaoDto> cadastrar(@PathVariable Long idUsuario, @RequestBody @Valid CartaoForm form, UriComponentsBuilder uriBuilder){
		Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
		if(usuario.isPresent()) {
			Cartao cartao = form.converter(usuario.get());
			cartaoRepository.save(cartao);
			URI uri = uriBuilder.path("/cartao/{id}").buildAndExpand(cartao.getId()).toUri();
			return ResponseEntity.created(uri).body(new CartaoDto(cartao));
		}
		return ResponseEntity.notFound().build();
	}
	
	
	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<DetalhesCartaoDto> atualizar(@PathVariable Long id, @RequestBody AtualizaCartaoForm form){
		Optional<Cartao> optional = cartaoRepository.findById(id);
		if(optional.isPresent()) {
			Cartao cartao = form.atualizar(optional.get());
			return ResponseEntity.ok(new DetalhesCartaoDto(cartao));
		}
		return ResponseEntity.notFound().build();
	}
	
	
	@CrossOrigin
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> remover(@PathVariable Long id){
		Optional<Cartao> optional = cartaoRepository.findById(id);
		if(optional.isPresent()) {
			cartaoRepository.deleteById(optional.get().getId());
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	

}
