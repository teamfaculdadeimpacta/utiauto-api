package br.com.utiauto.controller;

import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.utiauto.dto.usuario.UsuarioDto;
import br.com.utiauto.form.senha.AtualizaSenhaForm;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("/altera-senha")
public class AlteracaoSenhaController {

	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<UsuarioDto> atualizar(@PathVariable Long id, @RequestBody @Valid AtualizaSenhaForm form){
		Optional<Usuario> optional = usuarioRepository.findById(id);
		if (optional.isPresent() && optional.get().getSenha().equals(form.getSenha())) {
			form.atualizar(id, usuarioRepository);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	
	
}
