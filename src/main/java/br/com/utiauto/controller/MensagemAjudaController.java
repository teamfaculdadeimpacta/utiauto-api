package br.com.utiauto.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.utiauto.dto.mensagemajuda.DetalhesMensagemAjudaDto;
import br.com.utiauto.dto.mensagemajuda.MensagemAjudaDto;
import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.form.mensagemajuda.AtualizaMensagemAjudaForm;
import br.com.utiauto.form.mensagemajuda.MensagemAjudaForm;
import br.com.utiauto.modelo.MensagemAjuda;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.MensagemAjudaRepository;
import br.com.utiauto.repository.UsuarioRepository;
import br.com.utiauto.service.CrudControllerService;

@RequestMapping("/mensagem-ajuda")
@RestController
public class MensagemAjudaController implements CrudControllerService<MensagemAjuda, MensagemAjudaDto, DetalhesMensagemAjudaDto, MensagemAjudaForm, AtualizaMensagemAjudaForm> {

	@Autowired
	private MensagemAjudaRepository mensagemAjudaRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@CrossOrigin
	@GetMapping
	public List<MensagemAjudaDto> listar(Long idUsuario) {
		List<MensagemAjuda> mensagem;
		if (idUsuario == null) {
			mensagem = mensagemAjudaRepository.findByTipoMensagem(TipoMensagemEnum.AJUDA);
		} else {
			mensagem = mensagemAjudaRepository.findByUsuarioIdETipoMensagem(idUsuario, TipoMensagemEnum.AJUDA);
		}

		return MensagemAjudaDto.converter(mensagem);
	}

	@CrossOrigin
	@PostMapping("/{idUsuario}")
	@Transactional
	public ResponseEntity<MensagemAjudaDto> cadastrar(@PathVariable Long idUsuario, @RequestBody MensagemAjudaForm form,
			 UriComponentsBuilder uriBuilder) {
		Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
		if (usuario.isPresent()) {
			MensagemAjuda mensagem = form.converter(usuario.get());
			mensagemAjudaRepository.save(mensagem);
			URI uri = uriBuilder.path("/mensagem/{id}").buildAndExpand(mensagem.getId()).toUri();
			return ResponseEntity.created(uri).body(new MensagemAjudaDto(mensagem));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<DetalhesMensagemAjudaDto> atualizar(@PathVariable Long id,
			@RequestBody AtualizaMensagemAjudaForm form) {
		Optional<MensagemAjuda> optional = mensagemAjudaRepository.findById(id);
		if (optional.isPresent()) {
			MensagemAjuda mensagem = form.atualizar(optional.get());
			return ResponseEntity.ok(new DetalhesMensagemAjudaDto(mensagem));
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin
	@GetMapping("/{idUsuario}")
	public ResponseEntity<DetalhesMensagemAjudaDto> detalhar(@PathVariable Long idUsuario) {
		Optional<MensagemAjuda> optional = mensagemAjudaRepository.findByUsuarioId(idUsuario);
		if (optional.isPresent()) {
			return ResponseEntity.ok(new DetalhesMensagemAjudaDto(optional.get()));
		}
		return ResponseEntity.notFound().build();
	}



	@CrossOrigin
	@Transactional
	@DeleteMapping("/{id}")
	public ResponseEntity<MensagemAjuda> remover(Long id) {
		Optional<MensagemAjuda> optional = mensagemAjudaRepository.findById(id);
		if (optional.isPresent()) {
			mensagemAjudaRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}





}
