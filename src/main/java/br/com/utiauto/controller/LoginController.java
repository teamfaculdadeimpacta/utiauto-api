package br.com.utiauto.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.utiauto.dto.usuario.UsuarioDto;
import br.com.utiauto.form.login.LoginForm;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.UsuarioRepository;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@CrossOrigin
	@PostMapping
	public ResponseEntity<UsuarioDto> login(@RequestBody @Valid LoginForm loginForm){
		Optional<Usuario> optional = usuarioRepository.findByEmailSenha(loginForm.getEmail(), loginForm.getSenha());
		if (optional.isPresent()) {
			return ResponseEntity.ok(new UsuarioDto(optional.get()));
		}
		return ResponseEntity.notFound().build();	
	}

}
