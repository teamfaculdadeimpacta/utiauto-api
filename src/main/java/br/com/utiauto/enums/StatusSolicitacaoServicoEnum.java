package br.com.utiauto.enums;

public enum StatusSolicitacaoServicoEnum {
	APROVADO, 
	CANCELADO, 
	EMANDAMENTO,
	EMESPERA,
	AGUARDANDO_APROVACAO,
	EM_ANALISE,
	CANCELADO_PRESTADOR
}
