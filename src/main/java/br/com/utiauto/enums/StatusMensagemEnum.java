package br.com.utiauto.enums;

public enum StatusMensagemEnum {
	EMESPERA, 
	EMANDAMENTO,
	APROVADO, 
	CANCELADO
}
