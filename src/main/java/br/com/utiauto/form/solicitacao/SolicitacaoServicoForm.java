package br.com.utiauto.form.solicitacao;

import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.enums.TipoDePagamentoEnum;
import br.com.utiauto.form.enderecosolicitacao.EnderecoSolicitacaoForm;
import br.com.utiauto.modelo.Automovel;
import br.com.utiauto.modelo.EnderecoSolicitacao;
import br.com.utiauto.modelo.Servico;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.modelo.Usuario;

public class SolicitacaoServicoForm {

	private Long idServico;

	private Long idUsuario;

	private TipoDePagamentoEnum tipoDePagamento;

	private String[] localizacaoFinal;

	private EnderecoSolicitacaoForm enderecoSolicitacao;

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public TipoDePagamentoEnum getTipoDePagamento() {
		return tipoDePagamento;
	}

	public void setTipoDePagamento(TipoDePagamentoEnum tipoDePagamento) {
		this.tipoDePagamento = tipoDePagamento;
	}

	public String[] getLocalizacaoFinal() {
		return localizacaoFinal;
	}

	public void setLocalizacaoFinal(String[] localizacaoFinal) {
		this.localizacaoFinal = localizacaoFinal;
	}

	public EnderecoSolicitacaoForm getEnderecoSolicitacao() {
		return enderecoSolicitacao;
	}

	public void setEnderecoSolicitacao(EnderecoSolicitacaoForm enderecoSolicitacao) {
		this.enderecoSolicitacao = enderecoSolicitacao;
	}


	public SolicitacaoServico converter(Servico servico, Usuario usuario, Automovel automovel, EnderecoSolicitacao endereco) {
		return new SolicitacaoServico(servico, usuario, endereco, automovel,  StatusSolicitacaoServicoEnum.EMESPERA,
				tipoDePagamento, localizacaoFinal);
	}

}
