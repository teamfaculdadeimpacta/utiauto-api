package br.com.utiauto.form.solicitacao;

import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.modelo.SolicitacaoServico;

public class AtualizacaoSolicitacaoServicoUsuarioForm {


	private StatusSolicitacaoServicoEnum status;


	public StatusSolicitacaoServicoEnum getStatus() {
		return status;
	}


	public void setStatus(StatusSolicitacaoServicoEnum status) {
		this.status = status;
	}

	public SolicitacaoServico atualizar(SolicitacaoServico solicitacaoServico) {
		solicitacaoServico.setStatus(status);
		return solicitacaoServico;
	}
	
}
