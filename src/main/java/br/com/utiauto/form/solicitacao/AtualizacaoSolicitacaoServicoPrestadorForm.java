package br.com.utiauto.form.solicitacao;

import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.modelo.Usuario;

public class AtualizacaoSolicitacaoServicoPrestadorForm {

	private Long idPrestador;

	private StatusSolicitacaoServicoEnum status;

	private String[] localizacaoInicial;

	private String[] localizacaoAtual;
	
	

	public String[] getLocalizacaoInicial() {
		return localizacaoInicial;
	}

	public void setLocalizacaoInicial(String[] localizacaoInicial) {
		this.localizacaoInicial = localizacaoInicial;
	}

	public Long getIdPrestador() {
		return idPrestador;
	}

	public StatusSolicitacaoServicoEnum getStatus() {
		return status;
	}

	public void setIdPrestador(Long idPrestador) {
		this.idPrestador = idPrestador;
	}

	public void setStatus(StatusSolicitacaoServicoEnum status) {
		this.status = status;
	}


	public String[] getLocalizacaoAtual() {
		return localizacaoAtual;
	}

	public void setLocalizacaoAtual(String[] localizacaoAtual) {
		this.localizacaoAtual = localizacaoAtual;
	}

	public SolicitacaoServico atualizar(SolicitacaoServico solicitacaoServico, Usuario prestador) {
		solicitacaoServico.setPrestadorDeServico(prestador);
		solicitacaoServico.setStatus(status);
		solicitacaoServico.setLocalizacaoAtual(localizacaoAtual);
		if (verificaStatusLocalizacaoInicial(solicitacaoServico)) {
			solicitacaoServico.setLocalizacaoInicial(localizacaoInicial);
		}
		return solicitacaoServico;
	}

	public boolean verificaStatusLocalizacaoInicial(SolicitacaoServico solicitacaoServico) {
		return solicitacaoServico.getLocalizacaoInicial() == null ?  true : false;
	}

}
