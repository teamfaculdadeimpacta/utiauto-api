package br.com.utiauto.form.solicitacao;

import br.com.utiauto.modelo.SolicitacaoServico;

public class AtualizacaoSolicitacaoServicoPrestadorLocalizacaoForm {


	private String[] localizacaoAtual;


	public String[] getLocalizacaoAtual() {
		return localizacaoAtual;
	}

	public void setLocalizacaoAtual(String[] localizacaoAtual) {
		this.localizacaoAtual = localizacaoAtual;
	}

	public SolicitacaoServico atualizar(SolicitacaoServico solicitacaoServico) {
		solicitacaoServico.setLocalizacaoAtual(localizacaoAtual);
		return solicitacaoServico;
	}

}
