package br.com.utiauto.form.usuario;

import java.util.Optional;

import br.com.utiauto.modelo.Perfil;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.PerfilRepository;

public class AtualizacaoUsuarioParaPrestadorForm {

	public Usuario atualizar(Usuario usuario, PerfilRepository perfilRepository) throws Exception {
		Optional<Perfil> perfil = perfilRepository.findById(2L);
		usuario.setPerfil(perfil.get());
		return usuario;
	}

}
