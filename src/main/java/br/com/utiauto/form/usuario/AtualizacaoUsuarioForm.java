package br.com.utiauto.form.usuario;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.UsuarioRepository;
import br.com.utiauto.utils.EncodeDecodeUtils;

public class AtualizacaoUsuarioForm {

	@NotNull @NotEmpty
	private String nome;

	private String sobrenome;
	
	@NotNull @NotEmpty
	private String cpf;

	private String rg;

	private String dataNascimento;

	@NotNull @NotEmpty
	private String email;

	private String telefone;

	@NotNull @NotEmpty 
	private String celular;

	private String cnh;

	private String foto;
	
	private String tpArquivoFoto;
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}


	public String getTpArquivoFoto() {
		return tpArquivoFoto;
	}

	public void setTpArquivoFoto(String tpArquivoFoto) {
		this.tpArquivoFoto = tpArquivoFoto;
	}

	public Usuario atualizar(Long id, UsuarioRepository usuarioRepository) {
		EncodeDecodeUtils encode = new EncodeDecodeUtils();
		
		
		Usuario usuario = usuarioRepository.getOne(id);
		usuario.setNome(this.nome);
		usuario.setSobrenome(this.sobrenome);
		usuario.setDataNascimento(this.dataNascimento);
		usuario.setCelular(this.celular);
		usuario.setTelefone(this.telefone);
		usuario.setEmail(this.email);
		usuario.setCpf(this.cpf);
		usuario.setRg(this.rg);
		usuario.setCnh(this.cnh);
		
		try{
			usuario.setFoto(encode.decodeBase64ToByte(foto));
		} catch(Exception e) {
			System.out.println(e);
		}
		
		
		usuario.setTpArquivoFoto(tpArquivoFoto);
		return usuario;
	}

}
