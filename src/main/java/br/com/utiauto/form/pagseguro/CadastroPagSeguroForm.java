package br.com.utiauto.form.pagseguro;

import br.com.utiauto.modelo.CadastroPagSeguro;
import br.com.utiauto.modelo.Usuario;

public class CadastroPagSeguroForm {
	
	private String emailPagSeguro;
	
	private String password;
	
	private Long idUsuario;
	

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}


	/**
	 * @return the emailPagSeguro
	 */
	public String getEmailPagSeguro() {
		return emailPagSeguro;
	}

	/**
	 * @param emailPagSeguro the emailPagSeguro to set
	 */
	public void setEmailPagSeguro(String emailPagSeguro) {
		this.emailPagSeguro = emailPagSeguro;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public CadastroPagSeguro converter(Usuario usuario) {
		return new CadastroPagSeguro(emailPagSeguro, password, usuario);
	}

}
