package br.com.utiauto.form.pagseguro;

import br.com.utiauto.modelo.CadastroPagSeguro;
import br.com.utiauto.repository.CadastroPagSeguroRepository;

public class AtualizacaoCadastroPagSeguroForm {

	private String emailPagSeguro;

	private String password;

	/**
	 * @return the emailPagSeguro
	 */
	public String getEmailPagSeguro() {
		return emailPagSeguro;
	}

	/**
	 * @param emailPagSeguro the emailPagSeguro to set
	 */
	public void setEmailPagSeguro(String emailPagSeguro) {
		this.emailPagSeguro = emailPagSeguro;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CadastroPagSeguro atualizar(Long id, CadastroPagSeguroRepository cadastroPagSeguroRepository) {
		CadastroPagSeguro pagSeguro = cadastroPagSeguroRepository.getOne(id);
		pagSeguro.setEmailPagSeguro(this.emailPagSeguro);
		pagSeguro.setPassword(this.password);
		return pagSeguro;
	}

}
