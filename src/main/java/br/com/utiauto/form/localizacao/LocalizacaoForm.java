package br.com.utiauto.form.localizacao;

import br.com.utiauto.dto.localizacao.LocalizacaoDto;

public class LocalizacaoForm {
	
	public LocalizacaoForm() {
	}
	
	public LocalizacaoForm(String lat, String lng) {
		this.lat = lat;
		this.lng = lng;
	}

	private String lat;
	
	private String lng;


	public LocalizacaoDto converter() {
		return new LocalizacaoDto(lat, lng);
	}
	

}
