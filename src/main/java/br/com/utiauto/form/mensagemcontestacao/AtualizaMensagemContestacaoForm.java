package br.com.utiauto.form.mensagemcontestacao;


import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.modelo.MensagemContestacao;

public class AtualizaMensagemContestacaoForm {
	
	
	private StatusMensagemEnum statusMensagem = StatusMensagemEnum.EMESPERA;


	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}

	public void setStatusMensagem(StatusMensagemEnum statusMensagem) {
		this.statusMensagem = statusMensagem;
	}

	public MensagemContestacao atualizar(MensagemContestacao mensagem) {
		mensagem.setStatusMensagem(statusMensagem);
		return mensagem;
	}


	
}
