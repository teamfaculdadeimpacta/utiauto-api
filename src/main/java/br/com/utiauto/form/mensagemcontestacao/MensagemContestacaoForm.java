package br.com.utiauto.form.mensagemcontestacao;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.modelo.MensagemContestacao;
import br.com.utiauto.modelo.SolicitacaoServico;

public class MensagemContestacaoForm {
	
	
	private String titulo;
	
	private String mensagem;
	
	private TipoMensagemEnum tipoMensagem = TipoMensagemEnum.RECLAMACAO;
	
	private StatusMensagemEnum statusMensagem = StatusMensagemEnum.EMESPERA;


	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public TipoMensagemEnum getTipoMensagem() {
		return tipoMensagem;
	}

	public void setTipoMensagem(TipoMensagemEnum tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	
	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}

	public void setStatusMensagem(StatusMensagemEnum statusMensagem) {
		this.statusMensagem = statusMensagem;
	}

	public MensagemContestacao converter(SolicitacaoServico solicitacaoServico) {
		return new MensagemContestacao(titulo, mensagem, tipoMensagem, statusMensagem, solicitacaoServico);
	}

}
