package br.com.utiauto.form.senha;

import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.UsuarioRepository;

public class AtualizaSenhaForm {
	
	private String senhaNova;
	
	public String getSenhaNova() {
		return senhaNova;
	}


	public void setSenhaNova(String senhaNova) {
		this.senhaNova = senhaNova;
	}


	private String senha;

	
	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public Usuario atualizar(Long id, UsuarioRepository usuarioRepository) {
		Usuario usuario = usuarioRepository.getOne(id);
		usuario.setSenha(this.senha);
		return usuario;
	}

	
}
