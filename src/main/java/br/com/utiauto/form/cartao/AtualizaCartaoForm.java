package br.com.utiauto.form.cartao;

import br.com.utiauto.enums.TipoCartaoEnum;
import br.com.utiauto.modelo.Cartao;

public class AtualizaCartaoForm {
	
	private Long nrCartao;
	private String dtVencimento;
	private int codSeg;
	private String pais;
	private TipoCartaoEnum tpCartao;
	
	public Long getNrCartao() {
		return nrCartao;
	}
	public void setNrCartao(Long nrCartao) {
		this.nrCartao = nrCartao;
	}
	public String getDtVencimento() {
		return dtVencimento;
	}
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public int getCodSeg() {
		return codSeg;
	}
	public void setCodSeg(int codSeg) {
		this.codSeg = codSeg;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}

	public TipoCartaoEnum getTpCartao() {
		return tpCartao;
	}
	public void setTpCartao(TipoCartaoEnum tpCartao) {
		this.tpCartao = tpCartao;
	}
	

	public Cartao atualizar(Cartao cartao) {
		cartao.setCodSeg(codSeg);
		cartao.setDtVencimento(dtVencimento);
		cartao.setNrCartao(nrCartao);
		cartao.setPais(pais);
		cartao.setTpCartao(tpCartao);
		return cartao;
	}
	

}
