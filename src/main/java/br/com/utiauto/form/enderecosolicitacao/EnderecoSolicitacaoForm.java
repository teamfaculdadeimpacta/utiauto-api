package br.com.utiauto.form.enderecosolicitacao;

import br.com.utiauto.modelo.EnderecoSolicitacao;

public class EnderecoSolicitacaoForm {
	
	private String logradouro;

	private String numero;
	
	private String complemento;
	
	private String bairro;

	private String localidade;

	private String uf;

	private String cep;
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}


	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public EnderecoSolicitacao converter(EnderecoSolicitacaoForm enderecoSolicitacao) {
		return new EnderecoSolicitacao(enderecoSolicitacao.getLogradouro(), enderecoSolicitacao.getNumero(), enderecoSolicitacao.getComplemento(), enderecoSolicitacao.getBairro(), enderecoSolicitacao.getLocalidade(), enderecoSolicitacao.getUf(), enderecoSolicitacao.getCep());
	}

}
