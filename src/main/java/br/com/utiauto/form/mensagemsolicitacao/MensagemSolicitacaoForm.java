package br.com.utiauto.form.mensagemsolicitacao;

import br.com.utiauto.modelo.MensagemSolicitacao;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.modelo.Usuario;

public class MensagemSolicitacaoForm {


	private String mensagem;

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}


	public MensagemSolicitacao converter(Usuario usuario, SolicitacaoServico solictacaoServico) {
		return new MensagemSolicitacao(usuario, mensagem, solictacaoServico);
	}

}
