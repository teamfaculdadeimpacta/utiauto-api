package br.com.utiauto.form.mensagemsolicitacao;

import br.com.utiauto.modelo.MensagemSolicitacao;
import br.com.utiauto.modelo.Usuario;

public class AtualizacaoMensagemSolicitacaoForm {

	private String mensagem;

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public MensagemSolicitacao atualizar(MensagemSolicitacao mensagemSolicitacao, Usuario usuario) {
		mensagemSolicitacao.setMensagem(this.mensagem);
		mensagemSolicitacao.setUsuario(usuario);
		return mensagemSolicitacao;
	}

}
