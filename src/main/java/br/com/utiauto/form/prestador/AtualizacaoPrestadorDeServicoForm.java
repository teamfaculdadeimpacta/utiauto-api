package br.com.utiauto.form.prestador;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.utiauto.modelo.Perfil;
import br.com.utiauto.modelo.Servico;
import br.com.utiauto.modelo.Usuario;
import br.com.utiauto.repository.UsuarioRepository;
import br.com.utiauto.utils.EncodeDecodeUtils;

public class AtualizacaoPrestadorDeServicoForm {

	@NotNull
	@NotEmpty
	private String nome;

	private String sobrenome;

	@NotNull
	@NotEmpty
	private String cpf;

	private String rg;

	private String dataNascimento;

	@NotNull
	@NotEmpty
	private String email;

	private String telefone;

	private String celular;

	private String cnh;

	private String descricao;

	private String foto;

	private String tpArquivoFoto;

	@NotNull
	@NotEmpty
	private List<Long> servicos;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getTpArquivoFoto() {
		return tpArquivoFoto;
	}

	public void setTpArquivoFoto(String tpArquivoFoto) {
		this.tpArquivoFoto = tpArquivoFoto;
	}

	/**
	 * @return the servicos
	 */
	public List<Long> getServicos() {
		return servicos;
	}

	/**
	 * @param servicos the servicos to set
	 */
	public void setServicos(List<Long> servicos) {
		this.servicos = servicos;
	}

	public Usuario atualizar(Long id, UsuarioRepository prestadorDeServicoRepository, Perfil perfil,
			List<Servico> servicos) {
		EncodeDecodeUtils encode = new EncodeDecodeUtils();

		Usuario prestador = prestadorDeServicoRepository.getOne(id);
		prestador.setNome(this.nome);
		prestador.setSobrenome(this.sobrenome);
		prestador.setDataNascimento(this.dataNascimento);
		prestador.setCelular(this.celular);
		prestador.setTelefone(this.telefone);
		prestador.setEmail(this.email);
		prestador.setCpf(this.cpf);
		prestador.setRg(this.rg);
		prestador.setCnh(this.cnh);

		prestador.setTpArquivoFoto(this.tpArquivoFoto);
		perfil.setServico(servicos);
		prestador.setPerfil(perfil);

		try {
			prestador.setFoto(encode.decodeBase64ToByte(foto));
		} catch (Exception e) {
			System.out.println(e);
		}

		return prestador;
	}

}
