package br.com.utiauto.form.mensagemajuda;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.modelo.MensagemAjuda;
import br.com.utiauto.modelo.Usuario;

public class MensagemAjudaForm {


	private String mensagem;


	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public MensagemAjuda converter(Usuario usuario) {
		return new MensagemAjuda(mensagem, TipoMensagemEnum.AJUDA, StatusMensagemEnum.EMESPERA, usuario);
	}

}
