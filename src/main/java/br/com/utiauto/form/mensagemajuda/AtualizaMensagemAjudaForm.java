package br.com.utiauto.form.mensagemajuda;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.modelo.MensagemAjuda;

public class AtualizaMensagemAjudaForm {
	
	
	private StatusMensagemEnum statusMensagem = StatusMensagemEnum.EMESPERA;


	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}

	public void setStatusMensagem(StatusMensagemEnum statusMensagem) {
		this.statusMensagem = statusMensagem;
	}

	public MensagemAjuda atualizar(MensagemAjuda mensagem) {
		mensagem.setStatusMensagem(statusMensagem);
		return mensagem;
	}


	
}
