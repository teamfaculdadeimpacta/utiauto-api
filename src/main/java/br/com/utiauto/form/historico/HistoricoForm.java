package br.com.utiauto.form.historico;

import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;

public class HistoricoForm {

	
	private Long idUsuario;
	
	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	private StatusSolicitacaoServicoEnum statusSolicitacaoServicoEnum;



	public StatusSolicitacaoServicoEnum getStatusSolicitacaoServicoEnum() {
		return statusSolicitacaoServicoEnum;
	}

	public void setStatusSolicitacaoServicoEnum(StatusSolicitacaoServicoEnum statusSolicitacaoServicoEnum) {
		this.statusSolicitacaoServicoEnum = statusSolicitacaoServicoEnum;
	}
	

}
