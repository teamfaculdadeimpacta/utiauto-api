package br.com.utiauto.form.endereco;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.utiauto.modelo.Endereco;

public class AtualizacaoEnderecoForm {
	
	@NotNull @NotEmpty @Length(min = 1)
	private String logradouro;

	@NotNull @NotEmpty @Length(min = 1)
	private String numero;
	
	private String complemento;
	
	private String bairro;

	@NotNull @NotEmpty @Length(min = 1)
	private String localidade;

	@NotNull @NotEmpty @Length(min = 1)
	private String uf;

	@NotNull @NotEmpty @Length(min = 9)
	private String cep;

	


	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public Endereco atualizar(Endereco endereco) {
		
		endereco.setRua(logradouro);
		endereco.setCep(cep);
		endereco.setLocalidade(localidade);
		endereco.setComplemento(complemento);
		endereco.setUf(uf);
		endereco.setNumero(numero);
		endereco.setBairro(bairro);
		return endereco;
	}
	
	
	

}
