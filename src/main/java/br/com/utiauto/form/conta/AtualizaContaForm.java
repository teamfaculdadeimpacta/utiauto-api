package br.com.utiauto.form.conta;

import br.com.utiauto.modelo.Conta;
import br.com.utiauto.repository.ContaRepository;

public class AtualizaContaForm {
	
	private Long saldo;
	
	
	public Long getSaldo() {
		return saldo;
	}


	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}


	public Conta atualizar(Long id, ContaRepository contaRepository) {
		Conta conta = contaRepository.getOne(id);
		conta.setSaldo(conta.getSaldo() + saldo);
		return conta;
	}

}
