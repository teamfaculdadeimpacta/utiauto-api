package br.com.utiauto.form.conta;

import br.com.utiauto.modelo.Conta;
import br.com.utiauto.modelo.Usuario;

public class ContaForm {
	
	private Long idPrestador;
	
	private Long saldo;
	
	
	
	public Long getSaldo() {
		return saldo;
	}



	public Long getIdPrestador() {
		return idPrestador;
	}



	public void setIdPrestador(Long idPrestador) {
		this.idPrestador = idPrestador;
	}



	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}


	public Conta converter(Usuario prestador) {
		return new Conta(saldo, prestador);
	}

}
