package br.com.utiauto.dto.solicitacao;

import br.com.utiauto.dto.automovel.AutomovelDto;
import br.com.utiauto.dto.enderecosolicitacao.EnderecoSolicitacaoDto;
import br.com.utiauto.dto.localizacao.LocalizacaoDto;
import br.com.utiauto.dto.prestador.PrestadorDeServicoDto;
import br.com.utiauto.dto.servico.ServicoDto;
import br.com.utiauto.dto.usuario.UsuarioSolicitanteDto;
import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.enums.TipoDePagamentoEnum;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.service.LocalizacaoService;
import br.com.utiauto.service.impl.LocalizacaoServiceImpl;

public class DetalhesSolicitacaoServicoDto {
	
	private LocalizacaoService localizacaoServiceImpl = new LocalizacaoServiceImpl();
	
	private Long id;
	private ServicoDto servico;
	private UsuarioSolicitanteDto usuario;
	private PrestadorDeServicoDto prestador;
	private TipoDePagamentoEnum tipoDePagamentoEnum;
	private StatusSolicitacaoServicoEnum status;
	private LocalizacaoDto localizacaoFinal;
	private LocalizacaoDto localizacaoInicial;
	private LocalizacaoDto localizacaoAtual;
	private EnderecoSolicitacaoDto enderecoSolicitacao;
	private AutomovelDto automovel;


	public DetalhesSolicitacaoServicoDto(SolicitacaoServico servico) {
		this.id = servico.getId();
		this.servico = new ServicoDto(servico.getServico());
		this.usuario = new UsuarioSolicitanteDto(servico.getUsuario());
		verificaPrestador(servico);
		this.automovel = new AutomovelDto(servico.getAutomovel());
		this.enderecoSolicitacao = new EnderecoSolicitacaoDto(servico.getEnderecoSolicitacao());
		this.status = servico.getStatus();
		this.tipoDePagamentoEnum = servico.getTipoDePagamento();
		this.localizacaoFinal = localizacaoServiceImpl.setaLocalizacao(servico.getLocalizacaoFinal());
		this.localizacaoInicial = localizacaoServiceImpl.setaLocalizacao(servico.getLocalizacaoInicial());
		this.localizacaoAtual = localizacaoServiceImpl.setaLocalizacao(servico.getLocalizacaoAtual());

	}

	private void verificaPrestador(SolicitacaoServico servico) {
		if(servico.getPrestadorDeServico() != null) {
			this.prestador = new PrestadorDeServicoDto(servico.getPrestadorDeServico());
		}
	}
	
	public Long getId() {
		return id;
	}

	public TipoDePagamentoEnum getTipoDePagamentoEnum() {
		return tipoDePagamentoEnum;
	}

	public StatusSolicitacaoServicoEnum getStatus() {
		return status;
	}


	public ServicoDto getServico() {
		return servico;
	}

	public UsuarioSolicitanteDto getUsuario() {
		return usuario;
	}


	public PrestadorDeServicoDto getPrestador() {
		return prestador;
	}


	public LocalizacaoDto getLocalizacaoFinal() {
		return localizacaoFinal;
	}


	public LocalizacaoDto getLocalizacaoInicial() {
		return localizacaoInicial;
	}


	public LocalizacaoDto getLocalizacaoAtual() {
		return localizacaoAtual;
	}

	public LocalizacaoService getLocalizacaoServiceImpl() {
		return localizacaoServiceImpl;
	}

	public EnderecoSolicitacaoDto getEnderecoSolicitacao() {
		return enderecoSolicitacao;
	}


	public AutomovelDto getAutomovel() {
		return automovel;
	}

}
