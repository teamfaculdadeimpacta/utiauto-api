package br.com.utiauto.dto.solicitacao;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.dto.automovel.AutomovelDto;
import br.com.utiauto.dto.enderecosolicitacao.EnderecoSolicitacaoDto;
import br.com.utiauto.dto.localizacao.LocalizacaoDto;
import br.com.utiauto.dto.servico.ServicoDto;
import br.com.utiauto.dto.usuario.UsuarioSolicitanteDto;
import br.com.utiauto.enums.StatusSolicitacaoServicoEnum;
import br.com.utiauto.enums.TipoDePagamentoEnum;
import br.com.utiauto.modelo.SolicitacaoServico;
import br.com.utiauto.service.LocalizacaoService;
import br.com.utiauto.service.impl.LocalizacaoServiceImpl;

public class SolicitacaoServicoSolicitadoDto  {
	
	private LocalizacaoService localizacaoServiceImpl = new LocalizacaoServiceImpl();
	
	private Long id;
	private ServicoDto servico;
	private UsuarioSolicitanteDto usuario;
	private TipoDePagamentoEnum tipoDePagamentoEnum;
	private StatusSolicitacaoServicoEnum status;
	private UsuarioSolicitanteDto prestador;
	private LocalizacaoDto localizacaoFinal;
	private LocalizacaoDto localizacaoInicial;
	private LocalizacaoDto localizacaoAtual;
	private EnderecoSolicitacaoDto enderecoSolicitacao;
	private AutomovelDto automovel;
	

	public SolicitacaoServicoSolicitadoDto(SolicitacaoServico servico) {
		this.id = servico.getId();
		this.servico = new ServicoDto(servico.getServico());
		this.usuario = new UsuarioSolicitanteDto(servico.getUsuario());
		if(servico.getPrestadorDeServico() != null) {
			this.prestador = new UsuarioSolicitanteDto(servico.getPrestadorDeServico());
		}
		this.automovel = new AutomovelDto(servico.getAutomovel());
		this.enderecoSolicitacao = new EnderecoSolicitacaoDto(servico.getEnderecoSolicitacao());
		this.status = servico.getStatus();
		this.tipoDePagamentoEnum = servico.getTipoDePagamento();
		this.localizacaoAtual = localizacaoServiceImpl.setaLocalizacao(servico.getLocalizacaoAtual());
		this.localizacaoInicial = localizacaoServiceImpl.setaLocalizacao(servico.getLocalizacaoInicial());
		this.localizacaoFinal = localizacaoServiceImpl.setaLocalizacao(servico.getLocalizacaoFinal());
		
	}

	public TipoDePagamentoEnum getTipoDePagamentoEnum() {
		return tipoDePagamentoEnum;
	}
	
	
	public StatusSolicitacaoServicoEnum getStatus() {
		return status;
	}

	public Long getId() {
		return id;
	}

	public ServicoDto getServico() {
		return servico;
	}

	public UsuarioSolicitanteDto getUsuario() {
		return usuario;
	}
	

	public UsuarioSolicitanteDto getPrestador() {
		return prestador;
	}

	public LocalizacaoDto getLocalizacaoFinal() {
		return localizacaoFinal;
	}

	public LocalizacaoDto getLocalizacaoInicial() {
		return localizacaoInicial;
	}

	public LocalizacaoDto getLocalizacaoAtual() {
		return localizacaoAtual;
	}
	
	

	public LocalizacaoService getLocalizacaoServiceImpl() {
		return localizacaoServiceImpl;
	}


	public EnderecoSolicitacaoDto getEnderecoSolicitacao() {
		return enderecoSolicitacao;
	}
	

	public AutomovelDto getAutomovel() {
		return automovel;
	}


	public static List<SolicitacaoServicoDto> converter(List<SolicitacaoServico> servicos) {
		return servicos.stream().map(SolicitacaoServicoDto::new).collect(Collectors.toList());
	}

}
