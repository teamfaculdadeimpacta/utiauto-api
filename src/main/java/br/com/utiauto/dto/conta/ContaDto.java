package br.com.utiauto.dto.conta;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.Conta;

public class ContaDto {
	
	
	public ContaDto(Conta conta) {
		this.saldo = conta.getSaldo();
		this.id = conta.getId();
	}
	
	private Long id;
	
	private Long saldo;
	

	public Long getSaldo() {
		return saldo;
	}

	public Long getId() {
		return id;
	}


	public static List<ContaDto> converter(List<Conta> conta) {
		return conta.stream().map(ContaDto::new).collect(Collectors.toList());
	}

}
