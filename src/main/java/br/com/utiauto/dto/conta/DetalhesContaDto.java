package br.com.utiauto.dto.conta;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.Conta;

public class DetalhesContaDto {
	
	
	public DetalhesContaDto(Conta conta) {
		this.id = conta.getId();
		this.saldo = conta.getSaldo();
	}

	private Long id;
	
	private Long saldo;

	
	public Long getId() {
		return id;
	}


	public Long getSaldo() {
		return saldo;
	}


	public static List<DetalhesContaDto> converter(List<Conta> conta){
		return conta.stream().map(DetalhesContaDto::new).collect(Collectors.toList());
	}
}
