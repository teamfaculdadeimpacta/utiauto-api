package br.com.utiauto.dto.mensagemcontestacao;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.modelo.MensagemContestacao;

public class MensagemDto {
	

	public MensagemDto(MensagemContestacao mensagem) {
		this.titulo = mensagem.getTitulo();
		this.mensagem = mensagem.getMensagem();
		this.tipoMensagem = mensagem.getTipoMensagem();
		this.statusMensagem = mensagem.getStatusMensagem();
	}

	private String titulo;
	
	private String mensagem;
	
	private TipoMensagemEnum tipoMensagem;
	
	private StatusMensagemEnum statusMensagem;

	

	public TipoMensagemEnum getTipoMensagem() {
		return tipoMensagem;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}

	public static List<MensagemDto> converter(List<MensagemContestacao> mensagem) {
		return mensagem.stream().map(MensagemDto::new).collect(Collectors.toList());
	}
	
	
		

}
