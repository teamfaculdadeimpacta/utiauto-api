package br.com.utiauto.dto.mensagemcontestacao;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.modelo.MensagemContestacao;

public class DetalhesMensagemDto {
	
	
	public DetalhesMensagemDto(MensagemContestacao mensagem) {
		this.titulo = mensagem.getTitulo();
		this.mensagem = mensagem.getMensagem();
		this.tipoMensagem = mensagem.getTipoMensagem();
		this.statusMensagem = mensagem.getStatusMensagem();
	}

	private String titulo;
	
	private String mensagem;
	
	private TipoMensagemEnum tipoMensagem;
	
	private StatusMensagemEnum statusMensagem;

	public String getTitulo() {
		return titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public TipoMensagemEnum getTipoMensagem() {
		return tipoMensagem;
	}

	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}
	

}
