package br.com.utiauto.dto.mensagemajuda;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.modelo.MensagemAjuda;

public class MensagemAjudaDto {

	public MensagemAjudaDto(MensagemAjuda mensagem) {
		this.mensagem = mensagem.getMensagem();
		this.tipoMensagem = mensagem.getTipoMensagem();
		this.statusMensagem = mensagem.getStatusMensagem();
		this.id = mensagem.getId();
	}

	private Long id;

	private String mensagem;

	private TipoMensagemEnum tipoMensagem;

	private StatusMensagemEnum statusMensagem;

	public TipoMensagemEnum getTipoMensagem() {
		return tipoMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}

	public Long getId() {
		return id;
	}

	public static List<MensagemAjudaDto> converter(List<MensagemAjuda> mensagem) {
		return mensagem.stream().map(MensagemAjudaDto::new).collect(Collectors.toList());
	}

}
