package br.com.utiauto.dto.mensagemajuda;

import br.com.utiauto.enums.StatusMensagemEnum;
import br.com.utiauto.enums.TipoMensagemEnum;
import br.com.utiauto.modelo.MensagemAjuda;

public class DetalhesMensagemAjudaDto {
	
	
	public DetalhesMensagemAjudaDto(MensagemAjuda mensagem) {
		this.mensagem = mensagem.getMensagem();
		this.tipoMensagem = mensagem.getTipoMensagem();
		this.statusMensagem = mensagem.getStatusMensagem();
	}

	
	private String mensagem;
	
	private TipoMensagemEnum tipoMensagem;
	
	private StatusMensagemEnum statusMensagem;

	
	public String getMensagem() {
		return mensagem;
	}

	public TipoMensagemEnum getTipoMensagem() {
		return tipoMensagem;
	}

	public StatusMensagemEnum getStatusMensagem() {
		return statusMensagem;
	}
	

}
