package br.com.utiauto.dto.servico;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.Servico;

public class ServicoDto {

	private Long id;

	private String nomeServico;

	private String iconeServico;

	private double valorServico;

	public ServicoDto(Servico servico) {
		this.id = servico.getId();
		this.nomeServico = servico.getNomeServico();
		this.iconeServico = servico.getIconeServico();
		this.valorServico = servico.getValorServico();

	}

	public Long getId() {
		return id;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public double getValorServico() {
		return valorServico;
	}

	public String getIconeServico() {
		return iconeServico;
	}

	public static List<ServicoDto> converter(List<Servico> servicos) {
		return servicos.stream().map(ServicoDto::new).collect(Collectors.toList());
	}

}
