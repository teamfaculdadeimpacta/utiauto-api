package br.com.utiauto.dto.servico;

import br.com.utiauto.modelo.Servico;

public class DetalhesDoServicoDto {

	private Long id;

	private String nomeServico;

	private String iconeServico;

	private double valorServico;

	public DetalhesDoServicoDto(Servico servico) {
		this.id = servico.getId();
		this.nomeServico = servico.getNomeServico();
		this.iconeServico = servico.getIconeServico();
		this.valorServico = servico.getValorServico();
	}

	public Long getId() {
		return id;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public String getIconeServico() {
		return iconeServico;
	}

	public double getValorServico() {
		return valorServico;
	}

}
