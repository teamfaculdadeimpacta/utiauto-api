package br.com.utiauto.dto.usuario;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.dto.perfil.PerfilDto;
import br.com.utiauto.modelo.Usuario;

public class UsuarioDto {

	private Long id;
	private String nome;
	private String sobrenome;
	private String cpf;
	private String rg;
	private String dataNascimento;
	private String email;
	private String telefone;
	private String celular;
	private String cnh;
	private PerfilDto perfil;
	private byte[] foto;
	private String tpArquivoFoto;

	public UsuarioDto(Usuario usuario) {
		this.id = usuario.getId();
		this.nome = usuario.getNome();
		this.sobrenome = usuario.getSobrenome();
		this.cpf = usuario.getCpf();
		this.rg = usuario.getRg();
		this.dataNascimento = usuario.getDataNascimento();
		this.email = usuario.getEmail();
		this.telefone = usuario.getTelefone();
		this.celular = usuario.getCelular();
		this.cnh = usuario.getCnh();
		this.foto = usuario.getFoto();
		this.tpArquivoFoto = usuario.getTpArquivoFoto();
		this.perfil = new PerfilDto(usuario.getPerfil());

	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public String getCpf() {
		return cpf;
	}

	public String getRg() {
		return rg;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getCelular() {
		return celular;
	}

	public String getCnh() {
		return cnh;
	}

	public PerfilDto getPerfil() {
		return perfil;
	}

	public byte[] getFoto() {
		return foto;
	}

	public String getTpArquivoFoto() {
		return tpArquivoFoto;
	}

	public static List<UsuarioDto> converter(List<Usuario> usuario) {
		return usuario.stream().map(UsuarioDto::new).collect(Collectors.toList());
	}

}
