package br.com.utiauto.dto.usuario;

import br.com.utiauto.modelo.Perfil;
import br.com.utiauto.modelo.Usuario;

public class DetalhesDoUsuarioDto {
	
	private String nome;
	private String sobrenome;
	private String dataNascimento;
	private String email;
	private String telefone;
	private String celular;
	private String cnh;
	private Perfil perfil;
	private String cpf;
	private String rg;
	private byte[] foto;
	private String tpArquivoFoto;

	

	public DetalhesDoUsuarioDto(Usuario usuario) {
		this.nome = usuario.getNome();
		this.sobrenome = usuario.getSobrenome();
		this.dataNascimento = usuario.getDataNascimento();
		this.email = usuario.getEmail();
		this.telefone = usuario.getTelefone();
		this.celular = usuario.getCelular();
		this.cnh = usuario.getCnh();
		this.perfil = usuario.getPerfil();
		this.cpf = usuario.getCpf();
		this.rg = usuario.getRg();
		this.foto = usuario.getFoto();
		this.tpArquivoFoto = usuario.getTpArquivoFoto();
	}
	
	
	
	public String getTpArquivoFoto() {
		return tpArquivoFoto;
	}

	public String getNome() {
		return nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public String getEmail() {
		return email;
	}
	
	public String getTelefone() {
		return telefone;
	}	
	
	public String getCelular() {
		return celular;
	}
	public String getCnh() {
		return cnh;
	}

	
	public Perfil getPerfil() {
		return perfil;
	}


	public String getCpf() {
		return cpf;
	}


	public String getRg() {
		return rg;
	}


	public byte[] getFoto() {
		return foto;
	}
	
	

	

}
