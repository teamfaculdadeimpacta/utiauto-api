package br.com.utiauto.dto.usuario;

import br.com.utiauto.modelo.Usuario;

public class UsuarioSolicitanteDto {
	
	

	public UsuarioSolicitanteDto(Usuario usuario) {
		this.id = usuario.getId();
		this.nome = usuario.getNome();
		this.sobrenome = usuario.getSobrenome();
		this.cpf = usuario.getCpf();
		this.email = usuario.getEmail();
		this.celular = usuario.getCelular();
	}
	
	private Long id;
	private String nome;
	private String sobrenome;
	private String cpf;
	private String email;
	private String celular;

	
	public String getNome() {
		return nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public String getCpf() {
		return cpf;
	}
	public String getEmail() {
		return email;
	}
	public String getCelular() {
		return celular;
	}
	public Long getId() {
		return id;
	}
	
	
}
