package br.com.utiauto.dto.enderecosolicitacao;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.EnderecoSolicitacao;

public class EnderecoSolicitacaoDto {

	private Long id;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String localidade;
	private String uf;
	private String cep;

	public EnderecoSolicitacaoDto(EnderecoSolicitacao enderecoSolicitacao) {
		this.id = enderecoSolicitacao.getId();
		this.logradouro = enderecoSolicitacao.getRua();
		this.numero = enderecoSolicitacao.getNumero();
		this.complemento = enderecoSolicitacao.getComplemento();
		this.bairro = enderecoSolicitacao.getBairro();
		this.localidade = enderecoSolicitacao.getLocalidade();
		this.uf = enderecoSolicitacao.getUf();
		this.cep = enderecoSolicitacao.getCep();
	}

	public Long getId() {
		return id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public String getLocalidade() {
		return localidade;
	}

	public String getUf() {
		return uf;
	}

	public String getCep() {
		return cep;
	}

	public static List<EnderecoSolicitacaoDto> converter(List<EnderecoSolicitacao> enderecos) {
		return enderecos.stream().map(EnderecoSolicitacaoDto::new).collect(Collectors.toList());
	}

}
