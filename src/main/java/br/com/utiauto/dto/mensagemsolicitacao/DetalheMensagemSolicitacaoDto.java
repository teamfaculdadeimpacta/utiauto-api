package br.com.utiauto.dto.mensagemsolicitacao;

import java.time.LocalDateTime;

import br.com.utiauto.modelo.MensagemSolicitacao;
import br.com.utiauto.modelo.Usuario;

public class DetalheMensagemSolicitacaoDto {


	public DetalheMensagemSolicitacaoDto(MensagemSolicitacao mensagemSolicitacao) {
		this.usuario = mensagemSolicitacao.getUsuario();
		this.mensagem = mensagemSolicitacao.getMensagem();
		this.dataHora = mensagemSolicitacao.getDataCriacao();
	}

	private Usuario usuario;

	private String mensagem;

	private LocalDateTime dataHora;

	public Usuario getUsuario() {
		return usuario;
	}

	public String getMensagem() {
		return mensagem;
	}

	public LocalDateTime getDataHora() {
		return dataHora;
	}
}
