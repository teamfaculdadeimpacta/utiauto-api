package br.com.utiauto.dto.mensagemsolicitacao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.MensagemSolicitacao;

public class MensagemSolicitacaoDto {

	public MensagemSolicitacaoDto(MensagemSolicitacao mensagemSolicitacao) {
		this.idUsuario = mensagemSolicitacao.getUsuario().getId();
		this.nomeUsuario = mensagemSolicitacao.getUsuario().getNome() + " " + mensagemSolicitacao.getUsuario().getSobrenome();
		this.mensagem = mensagemSolicitacao.getMensagem();
		this.dataHora = mensagemSolicitacao.getDataCriacao();
	}
	
	private String nomeUsuario;

	private Long idUsuario;

	private String mensagem;

	private LocalDateTime dataHora;


	public String getMensagem() {
		return mensagem;
	}

	public LocalDateTime getDataHora() {
		return dataHora;
	}
	
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	
	public Long getIdUsuario() {
		return idUsuario;
	}

	public static List<MensagemSolicitacaoDto> converter(List<MensagemSolicitacao> mensagemSolicitacao) {
		return mensagemSolicitacao.stream().map(MensagemSolicitacaoDto::new).collect(Collectors.toList());
	}

}
