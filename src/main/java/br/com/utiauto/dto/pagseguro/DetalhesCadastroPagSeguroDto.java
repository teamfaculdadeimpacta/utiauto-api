package br.com.utiauto.dto.pagseguro;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.CadastroPagSeguro;

public class DetalhesCadastroPagSeguroDto {
	
	
	private String emailPagSeguro;
	
	private String password;
	


	public DetalhesCadastroPagSeguroDto(CadastroPagSeguro cadastroPagSeguro) {
		this.emailPagSeguro = cadastroPagSeguro.getEmailPagSeguro();
		this.password = cadastroPagSeguro.getPassword();

	}

	/**
	 * @return the emailPagSeguro
	 */
	public String getEmailPagSeguro() {
		return emailPagSeguro;
	}


	public String getPassword() {
		return password;
	}
	


	
	public static List<DetalhesCadastroPagSeguroDto> converter(List<CadastroPagSeguro> cadastroPagSeguro){
		return cadastroPagSeguro.stream().map(DetalhesCadastroPagSeguroDto::new).collect(Collectors.toList());
	}
	
	
	

}
