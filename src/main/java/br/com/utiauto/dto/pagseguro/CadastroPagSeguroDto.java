package br.com.utiauto.dto.pagseguro;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.CadastroPagSeguro;

public class CadastroPagSeguroDto {
	
	
	private String emailPagSeguro;
	
	private String password;
	
	private Long id;


	public CadastroPagSeguroDto(CadastroPagSeguro cadastroPagSeguro) {
		this.emailPagSeguro = cadastroPagSeguro.getEmailPagSeguro();
		this.password = cadastroPagSeguro.getPassword();
		this.id = cadastroPagSeguro.getId();
	}


	/**
	 * @return the emailPagSeguro
	 */
	public String getEmailPagSeguro() {
		return emailPagSeguro;
	}



	public String getPassword() {
		return password;
	}
	
	public Long getId() {
		return id;
	}

	
	public static List<CadastroPagSeguroDto> converter(List<CadastroPagSeguro> cadastroPagSeguro){
		return cadastroPagSeguro.stream().map(CadastroPagSeguroDto::new).collect(Collectors.toList());
	}
	
	
	

}
