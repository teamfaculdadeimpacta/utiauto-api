package br.com.utiauto.dto.prestador;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.Perfil;
import br.com.utiauto.modelo.Usuario;

public class PrestadorDeServicoDto {

	private Long id;
	private String nome;
	private String sobrenome;
	private String cpf;
	private String rg;
	private String dataNascimento;
	private String email;
	private String telefone;
	private String celular;
	private String cnh;
	private Perfil perfil;
	private byte[] foto;
	private String tpArquivoFoto;

	public PrestadorDeServicoDto(Usuario prestador) {
		this.id = prestador.getId();
		this.nome = prestador.getNome();
		this.sobrenome = prestador.getSobrenome();
		this.cpf = prestador.getCpf();
		this.rg = prestador.getRg();
		this.dataNascimento = prestador.getDataNascimento();
		this.email = prestador.getEmail();
		this.telefone = prestador.getTelefone();
		this.celular = prestador.getCelular();
		this.cnh = prestador.getCnh();
		this.foto = prestador.getFoto();
		this.tpArquivoFoto = prestador.getTpArquivoFoto();
		this.perfil = prestador.getPerfil();
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public String getCpf() {
		return cpf;
	}

	public String getRg() {
		return rg;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getCelular() {
		return celular;
	}

	public String getCnh() {
		return cnh;
	}

	public byte[] getFoto() {
		return foto;
	}

	public Perfil getPerfil() {
		return perfil;
	}
	
	public String getTpArquivoFoto() {
		return tpArquivoFoto;
	}

	public static List<PrestadorDeServicoDto> converter(List<Usuario> prestador) {
		return prestador.stream().map(PrestadorDeServicoDto::new).collect(Collectors.toList());
	}

}
