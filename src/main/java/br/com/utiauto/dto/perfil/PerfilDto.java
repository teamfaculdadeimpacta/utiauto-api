package br.com.utiauto.dto.perfil;

import java.util.List;

import br.com.utiauto.dto.servico.ServicoDto;
import br.com.utiauto.modelo.Perfil;

public class PerfilDto {
	
	
	public PerfilDto(Perfil perfil) {
		this.id = perfil.getId();
		this.nome = perfil.getNome();
		this.servico = ServicoDto.converter(perfil.getServico());
	}

	private Long id; 
	
	private String nome;
	
	private List<ServicoDto> servico;


	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public List<ServicoDto> getServico() {
		return servico;
	}
	
	
	


	
}
