package br.com.utiauto.dto.endereco;

import br.com.utiauto.modelo.Endereco;

public class DetalhesDoEnderecoDto {

	private Long id;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String localidade;
	private String uf;
	private String cep;
	
	public DetalhesDoEnderecoDto(Endereco endereco) {
		this.id = endereco.getId();
		this.logradouro = endereco.getRua();
		this.numero = endereco.getNumero();
		this.complemento = endereco.getComplemento();
		this.bairro = endereco.getBairro();
		this.localidade = endereco.getLocalidade();
		this.uf = endereco.getUf();
		this.cep = endereco.getCep();
	}

	public Long getId() {
		return id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public String getLocalidade() {
		return localidade;
	}

	public String getUf() {
		return uf;
	}

	public String getCep() {
		return cep;
	}
	
	


}
