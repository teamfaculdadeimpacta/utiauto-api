package br.com.utiauto.dto.localizacao;

public class LocalizacaoDto {
	
	public LocalizacaoDto() {
	}
	
	public LocalizacaoDto(String lat, String lng) {
		this.lat = lat;
		this.lng = lng;
	}


	private String lat;
	
	private String lng;
	

	public String getLat() {
		return lat;
	}


	public String getLng() {
		return lng;
	}





}
