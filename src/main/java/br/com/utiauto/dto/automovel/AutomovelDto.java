package br.com.utiauto.dto.automovel;

import java.util.List;
import java.util.stream.Collectors;

import br.com.utiauto.modelo.Automovel;

public class AutomovelDto {

	private Long id;
	private String modelo;
	private int ano;
	private String marca;
	private Long renavam;
	private String cor;
	private String placa;

	public AutomovelDto(Automovel carro) {
		this.id = carro.getId();
		this.modelo = carro.getModelo();
		this.ano = carro.getAno();
		this.marca = carro.getMarca();
		this.renavam = carro.getRenavam();
		this.cor = carro.getCor();
		this.placa = carro.getPlaca();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}

	/**
	 * @return the ano
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}

	/**
	 * @return the renavam
	 */
	public Long getRenavam() {
		return renavam;
	}

	/**
	 * @return the cor
	 */
	public String getCor() {
		return cor;
	}

	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}

	public static List<AutomovelDto> converter(List<Automovel> carro) {
		return carro.stream().map(AutomovelDto::new).collect(Collectors.toList());
	}

}
