package br.com.utiauto.dto.cartao;

import br.com.utiauto.enums.TipoCartaoEnum;
import br.com.utiauto.modelo.Cartao;

public class DetalhesCartaoDto {

	public DetalhesCartaoDto(Cartao cartao) {
		this.id = cartao.getId();
		this.nrCartao = cartao.getNrCartao();
		this.dtVencimento = cartao.getDtVencimento();
		this.codSeg = cartao.getCodSeg();
		this.pais = cartao.getPais();
		this.tpCartao = cartao.getTpCartao();
		
	}

	private Long id;
	private Long nrCartao;
	private String dtVencimento;
	private int codSeg;
	private String pais;
	private TipoCartaoEnum tpCartao;

	public Long getNrCartao() {
		return nrCartao;
	}

	public String getDtVencimento() {
		return dtVencimento;
	}

	public int getCodSeg() {
		return codSeg;
	}

	public String getPais() {
		return pais;
	}

	public TipoCartaoEnum getTpCartao() {
		return tpCartao;
	}

	public Long getId() {
		return id;
	}
	
	


}
