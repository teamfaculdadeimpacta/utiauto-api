package br.com.utiauto.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;

import org.apache.log4j.Logger;

public class EncodeDecodeUtils {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	public String encodeFileToBase64Binary(File file) {
		FileInputStream fileInputStreamReader;
		byte[] bytes = null;
		try {
			fileInputStreamReader = new FileInputStream(file);
			bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
		} catch (Exception e) {
			logger.error("ERROR: ",  e);
		}
		return new String(Base64.getEncoder().encode((bytes)));
	}

	public byte[] encodeFileToByte(File file) {
		FileInputStream fileInputStreamReader;
		byte[] bytes = null;
		try {
			fileInputStreamReader = new FileInputStream(file);
			bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
		} catch (Exception e) {
			logger.error("ERROR: ",  e);
		}
		return bytes;
	}

	public byte[] decodeBase64ToByte(String data) {
		return Base64.getDecoder().decode(data);
	}

}
